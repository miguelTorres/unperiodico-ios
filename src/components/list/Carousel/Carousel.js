import React, { PureComponent } from 'react'
import {
  View,
  Dimensions,
  FlatList,
  RefreshControl
} from 'react-native'

import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  H3,
  Spinner,
} from "native-base";

import Carousel, { Pagination } from 'react-native-snap-carousel';

import HTML from 'react-native-render-html';
import config from '../../../../config'
import styleVariables from '../../../../native-base-theme/variables/unperiodico-material'
import CarouselItem from './CarouselItem'
import CustomRefreshControl from '../../CustomRefreshControl';

export default class CarouselView extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {activeSlide: 0};

  }
  componentDidMount() {
    this.isComponentMount = true
  }
  componentWillUnmount() 
  {
    this.isComponentMount = false
  }  


  _renderItem = (data) => (
    <View>
      <CarouselItem
        data={data}
      />
    </View>
  );  
  renderSeparator = () => {
    return null
    return (
      <View style={{justifyContent: 'center', flex: 1, flexDirection: 'row'}}>
        <View
          style={{
            height: 1,
            width: "80%",
            backgroundColor: "#CED0CE",
          }}
          />
      </View>
    )
  }
  renderRow = (data) => {
    return (
      <Item data={data} />
    )
  }
  renderFooter = () => {
    const hasNextPage = this.props.hasNextPage()


    if(!hasNextPage || this.props.isRefreshing()) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE",
        }}
        >
          <Spinner />
        </View>
    )
  }

  get pagination () {
    const { activeSlide } = this.state;
    const entries = this.props.data
    return (
        <Pagination
          dotsLength={entries.length}
          activeDotIndex={activeSlide}
          containerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.75)' }}
          dotStyle={{
              width: 10,
              height: 10,
              borderRadius: 5,
              marginHorizontal: 8,
              backgroundColor: 'rgba(255, 255, 255, 0.92)'
          }}
          inactiveDotStyle={{
              // Define styles for inactive dots here
          }}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
          carouselRef={this._carousel}
          tappableDots={!!this._carousel}
        />
    );
}  
  render() {
    const isRefreshing = this.props.isRefreshing()
    var {height, width} = Dimensions.get('window');
    const sliderWidth = width
    const itemWidth = width

    return (
      <View>
        <Carousel
          ref={(c) => { this._carousel = c; }}
          data={this.props.data}
          renderItem={this._renderItem}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
          onSnapToItem={(index) => this.isComponentMount && this.setState({ activeSlide: index }) }
        />
        { this.pagination }
      </View>
      
    );

    return(
      <FlatList
        data={this.props.data}
        renderItem={this.renderRow}
        keyExtractor={item=>""+item.uid}
        ItemSeparatorComponent={this.renderSeparator}
        ListFooterComponent={this.renderFooter}
        refreshing={isRefreshing}
        onRefresh={this.props.handleRefresh}
        onEndReached={this.props.onEndReached}
        
        refreshControl={
          <CustomRefreshControl
            colors={["red", "green", "blue"]}
            refreshing={true}
            onRefresh={this.props.onRefreshControl}
            />
        }          
        />   
      
    )
  }
}

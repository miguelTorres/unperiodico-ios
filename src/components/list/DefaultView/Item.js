import React, { PureComponent } from 'react'
import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  H3,
} from "native-base";
import HTML from 'react-native-render-html';
import config from '../../../../config'
import styleVariables from '../../../../native-base-theme/variables/unperiodico-material'
import styles from "./styles"
import appConfig from '../../../models/appConfig';
export default class Item extends PureComponent {


  render() {
    const item = this.props.data.item
    const img = appConfig.config.app.SITE_URL + item.additional.falMediaPreviews.src
    const title = '<p>' + item.title + '</p>'
    const description = '<p>' + item.title + '</p>'

    return (
      <ListItem thumbnail style={styles.listItem} >
                <Left style={{alignSelf: 'flex-start', paddingTop: 15}}>
                  <Thumbnail square size={55} source={{uri:img}} />
                </Left>
                <Body style={styles.listItemBody}>
                  <HTML html={title} tagsStyles = {{ p: { fontSize: 16, color: styleVariables.titleFontColor } }} />
                  <Text numberOfLines={1} note>
                    {item.title}
                  </Text>
                  <Button small transparent>
                    <Text>Ver</Text>
                  </Button>
                </Body>
              </ListItem>
    )

  }
}

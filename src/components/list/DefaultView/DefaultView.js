import React, { PureComponent } from 'react'
import {
  View,
  FlatList,
  RefreshControl
} from 'react-native'

import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  H3,
  Spinner,
} from "native-base";

import HTML from 'react-native-render-html';
import config from '../../../../config'
import styleVariables from '../../../../native-base-theme/variables/unperiodico-material'
import Item from './Item'
import CustomRefreshControl from '../../CustomRefreshControl';

export default class DefaultView extends PureComponent {

  _renderItem = (data) => (
    <Item
      data={data}
    />
  );  
  renderSeparator = () => {
    return null
    return (
      <View style={{justifyContent: 'center', flex: 1, flexDirection: 'row'}}>
        <View
          style={{
            height: 1,
            width: "80%",
            backgroundColor: "#CED0CE",
          }}
          />
      </View>
    )
  }
  renderRow = (data) => {
    return (
      <Item data={data} />
    )
  }
  renderFooter = () => {
    const hasNextPage = this.props.hasNextPage()
    return (
      <View><Text>Hola</Text></View>
    )

    //if(!hasNextPage || this.props.isRefreshing()) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE",
        }}
        >
          <Spinner />
        </View>
    )
  }
  render() {
    const isRefreshing = this.props.isRefreshing()
    return(
      <FlatList
        data={this.props.data}
        renderItem={this.renderRow}
        keyExtractor={item=>""+item.uid}
        ItemSeparatorComponent={this.renderSeparator}
        ListFooterComponent={this.renderFooter}
        refreshing={isRefreshing}
        onRefresh={this.props.handleRefresh}
        onEndReached={this.props.onEndReached}
        
        refreshControl={
          <CustomRefreshControl
            colors={["red", "green", "blue"]}
            refreshing={true}
            onRefresh={this.props.onRefreshControl}
            />
        }          
        />   
      
    )
  }
}

import React, {PureComponent, Component} from 'react';
import { StyleSheet, View, Alert, WebView } from 'react-native';

import {
  Text,
} from "native-base";
import configurationHelper from '../helpers/ConfigurationHelper';
import variableHeadingService from '../services/VariableHeadingService';
import { EventRegister } from 'react-native-event-listeners';
import HomeConstants from '../screens/home/HomeConstants';

export default class VariableHeading extends PureComponent {
  constructor(props){
    super(props)
    this.state = {}
  }
  componentDidMount() {
    this.isComponentMount = true
  }

  componentWillUnmount(){
    this.isComponentMount = false
    if(typeof this.timeState !== 'undefined'){
      clearTimeout(this.timeState)
    }
    
  }
  calculateLabel(footerTab){
    if(typeof this.props.newsByCategory !== 'undefined'){
      var news = this.props.newsByCategory
      var newsItem = typeof news.items !== 'undefined' && news.items.length > 0? news.items[0]:null
      if(newsItem !== null){
        const configuration = configurationHelper.getConfiguration(footerTab.title, newsItem.additional.categories)
        var title = (configuration && configuration.title)? configuration.title : footerTab.title.default.title
        var tag = this.props.category + ':' + title
        if(typeof variableHeadingService.getTitle(tag)==='undefined'){

          variableHeadingService.setTitle(tag, true)
          //this.label = title
          /*this.timeState = setTimeout(() => {
              this.isComponentMount && this.setState({
                label: title
              })
            }, 300);
          }*/
          //return title
          this.timeEventRefresh = setTimeout(() => {
              EventRegister.emit(HomeConstants.REFRESH_TAB_BAR, {category: this.props.category, tag, title})
          }, 300);
        }else{
          if(typeof variableHeadingService.getTitle(tag) === 'string'){
            this.label = variableHeadingService.getTitle(tag)
            return this.label  
          }
        }
        this.label = footerTab.title.default.title
        return this.label
      }
    }
    return footerTab.title.default.title
  }
  render() {
    const label = typeof this.label !== 'undefined'? this.label : 
    (this.props.connected? this.calculateLabel(this.props.navigationItem.label.footerTab):this.props.navigationItem.label)
    //const label = typeof this.props.navigationItem.label === 'object'? this.props.navigationItem.label.f(this.props.navigationItem.label.footerTab):this.props.navigationItem.label

    return (
      <Text style={this.props.textStyle}>{label}</Text>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    borderColor: 'red',
    borderWidth: 3,
    flexDirection: 'row'
  },
  icon: {
    fontSize: 40,
  }
});
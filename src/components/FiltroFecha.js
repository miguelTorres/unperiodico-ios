import React, { Component } from 'react'
import { View, Image, Dimensions, StyleSheet } from "react-native";
import { Text, Button } from "native-base";
import DatePicker from 'react-native-datepicker'

import moment from 'moment'
import 'moment/locale/es'
moment.locale('es')

import unperiodicoVariables from '../../native-base-theme/variables/unperiodico-material'

import appConfig from '../models/appConfig';
import config from '../../config'
import Assets from '../../assets'
import ButtonIcon from './ButtonIcon';
import { calculateResponsiveValue } from '../helpers/Responsive';

export default class FiltroFecha extends Component {
  constructor(props) {
    super(props)

  }
  renderDatePickerIcon(){
    const dateText = this.props.date? moment(this.props.date, 'YYYY-MM-DD').format(appConfig.config.app.SONDEOS.formatDate) : ''
    const fechaTexto = this.props.date? 'Hasta: '+ dateText : 'Filtrar por fecha...'
    return (
      <View style={styles.filtroFechaContainer}>
        <Text style={styles.filtroFechaTexto}>{fechaTexto}</Text>
        {!this.props.date && 
          <View style={styles.filtroFechaIconContainer}>
            <Image style={styles.filtroFecha} source={Assets.filtroFecha} />
          </View>
        }
        {!!this.props.date && 
          <View style={styles.filtroFechaIconContainer}>
            <ButtonIcon headerButton={true} imageStyle = {styles.filtroFecha} imageSource={Assets.equisRoja} onPress={this.props.onResetDate}>
            </ButtonIcon>
          </View>
        }
      </View>
    )
  }

  render() {
    return (
      <DatePicker
        style={{width: '100%', height: config.filtroFecha.height, paddingTop: 0}}
        customStyles={{
          dateTouchBody: styles.dateTouchBody, 
          dateTouch: styles.dateTouch
        }}
        androidMode="spinner"
        date={this.props.date}
        mode="date"
        hideText={true}
        iconComponent={this.renderDatePickerIcon()}
        placeholder="select date"
        format="YYYY-MM-DD"
        //minDate="2016-05-01"
        //maxDate="2016-06-01"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            /*position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0*/
          },
          dateInput: {
            marginLeft: 36
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={this.props.onDateChange}
        confirmBtnText="Confirmar"
        cancelBtnText="Cancelar"
      />
    )    
  }
}

const styles = StyleSheet.create({
  filtroFechaContainer: {
    backgroundColor: config.colors.config2.background,
    width: '100%',
    justifyContent: 'center',
    //alignItems: 'center',
    paddingLeft: calculateResponsiveValue(40),
    height: config.filtroFecha.height,
    paddingTop: 0,
  },
  filtroFechaIconContainer: {
    position: 'absolute',
    right: calculateResponsiveValue(20),
    top: calculateResponsiveValue(0, 10),
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  filtroFechaTexto: {
    fontSize: calculateResponsiveValue(14),
    color: config.colors.config2.color
  },
  filtroFecha:{
    width: calculateResponsiveValue(30),
    height: calculateResponsiveValue(30),
    resizeMode: 'contain'
  },
  headerButton: {
    borderWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },  
  dateTouchBody: {
    // Toca hacer este fix en la librería directamente
    height: 0,
  },
  dateTouch: {
  }

});



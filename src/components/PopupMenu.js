import React from 'react';
import {View,TouchableOpacity,UIManager,findNodeHandle, StyleSheet} from 'react-native';
import {Icon, Button, ActionSheet} from 'native-base';
import PropTypes from 'prop-types';
import { calculateResponsiveValue } from '../helpers/Responsive';

const ICON_SIZE = 24;

var BUTTONS = ["Option 0", "Option 1", "Option 2", "Delete", "Cancel"];
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;

const styles = StyleSheet.create({
    headerButton: {
        borderWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0,
      },
  });

class PopupMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  handleShowPopupError = () => {
    // show error here
  };

  handleMenuPress = () => {

    const { actions, onPress, cancelIndex } = this.props;

    ActionSheet.show(
      {
        options: actions,
        //cancelButtonIndex: cancelIndex,
        title: "Acciones"
      },
      buttonIndex => {
        onPress(null, buttonIndex)
      }
    )
      return


    UIManager.showPopupMenu(
      findNodeHandle(this.refs.menu),
      actions,
      this.handleShowPopupError,
      onPress,
    );
  };

  render() {
    return (
      <View>
        <Button bordered style={styles.headerButton} onPress={this.handleMenuPress}>
        <Icon
            name={this.props.icon.name}
            type={this.props.icon.type}
            //size={ICON_SIZE}
            //color='white'
            ref="menu"
            style={{fontSize: calculateResponsiveValue(25, 30)}}
          />
            </Button>
      </View>
    );
  }
}

PopupMenu.propTypes = {
  actions: PropTypes.array.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default PopupMenu;
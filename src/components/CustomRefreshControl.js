import React, { Component } from 'react'
import {StyleSheet, View} from 'react-native'
import {
  RefreshControl
} from "react-native";
import { calculateResponsiveValue } from '../helpers/Responsive';

import unperiodicoVariables from '../../native-base-theme/variables/unperiodico-material'
import config from '../../config'





export default class CustomRefreshControl extends Component {


  render() {

    return(
      <RefreshControl tintColor={config.colors.config1.background} {...this.props} />        
    )


  }
}
const styles = StyleSheet.create({
  header: {
    backgroundColor: config.colors.config1.background, 
    height: calculateResponsiveValue(54, 75),
  },
  innerHeader: {
    position: 'absolute',
    top: unperiodicoVariables.isIphoneX? 0 : 18,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#fff'
  }

});

import React from 'react';
import { StyleSheet, Text, View, Alert, WebView } from 'react-native';
import {
  Icon,
  Button
} from "native-base";
import { EventRegister } from 'react-native-event-listeners'
import FloatingViewConstants from './FloatingViewConstants'
import HTML from './HTML'
import VideoPlayer from './VideoPlayer';

export default class IframePlayer extends VideoPlayer {
  constructor(props){
    super(props)
    this.state = { maximized: false }
  }
  

  renderVideoComponent(width, height) {
    return (
      <View style={styles.container}>
        <HTML
          html={this.props.html}
          width={width}
          height={height}
          stylesConfiguration='detailConfiguration'
        />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  icon: {
    fontSize: 40,
  }
});
import React, { Component } from 'react'
import Carousel from 'react-native-carousel-view';
import Orientation from 'react-native-orientation';
import {
  Dimensions,
  View,
  StyleSheet,
} from "react-native";

const styles = StyleSheet.create({
  indicatorBackground: {
    position: 'absolute',
    width: '100%',
    height: 25,
    backgroundColor: 'rgba(128, 128, 128, 0.3)',
    bottom: 0,
  },
});
export default class UNCarousel extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dimensions: {}
    }
    this._orientationDidChange = this._orientationDidChange.bind(this)
    this.onRotated = this.onRotated.bind(this)
  }
  componentDidMount() {
    this.isComponentMount = true
    //Orientation.addOrientationListener(this._orientationDidChange);
    Dimensions.addEventListener('change', this.onRotated)
  }
  onRotated({ window: { width, height } }) {
    if(!this.props.isFullScreen){
      return
    }
    const orientation = width > height ? 'LANDSCAPE' : 'PORTRAIT'

    
    this.isComponentMount && this.setState({
      dimensions: {
        height,
        width
      }
    })
    
  }
  _orientationDidChange = (orientation) => {
    if(!this.props.isFullScreen){
      return
    }
    var {height, width} = Dimensions.get('window');
    if (orientation === 'LANDSCAPE') {
      if(width<height){
        [height, width] = [width, height]
      }
    } else {
      if(width>height){
        [height, width] = [width, height]
      }
    }
    
    this.isComponentMount && this.setState({
      dimensions: {
        height,
        width
      }
    })
  }
  componentWillUnmount() {
    this.isComponentMount = false
    Orientation.removeOrientationListener(this._orientationDidChange);
  }  
  renderItem(item, index){
    return (
      <View key={''+index}  >
        {this.props.renderItem(item, index)}
        <View style={styles.indicatorBackground}></View>
      </View>
    )
  }
  render() {
    
    return(
      <Carousel
            {...this.state.dimensions}
            {...this.props}
            ref={(carousel) => {
              this.carousel = carousel;
            }}
            indicatorSize = {20}
            >
            {this.props.data.map((item, index) => {
              return this.renderItem(item, index)
            })}
          </Carousel>
    )
  }
}


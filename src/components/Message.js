import React, { Component } from 'react'
import {
  View,
  StyleSheet,
} from "react-native";
import {
  Text
} from "native-base";



const styles = StyleSheet.create({
  container: {
  },
  offline: {
    backgroundColor: '#000000',
    color: '#FFFFFF',
    textAlign: 'center',
    paddingTop: 0,
  },
});


export default class Message extends Component {


  render() {

    if(!this.props.message){
      return null;
    }
    return(
      <View style={styles.container}>
        <Text style={styles.offline}>{this.props.message}</Text>
      </View>
    )


  }
}


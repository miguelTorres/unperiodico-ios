import React, { Component } from 'react'
import {Toast} from 'native-base'
import firebaseDatabase from '../models/firebaseDatabase';
import appService from '../services/appService';
import FCM from '../models/fcm'
import firebaseConstants from '../models/firebaseConstants';
import { EventRegister } from 'react-native-event-listeners';
import appConfig from '../models/appConfig';
import JsonApi from '../api/jsonApi';
import _ from "lodash";


export default class TransversalActions extends Component {
  constructor(props) {
    super(props)
    this.isManagedAppLoaded = false
    this.isAppLoaded = false
  }
  componentDidMount() {
    this.listenerAppLoaded = EventRegister.addEventListener(firebaseConstants.APP_LOADED, (value) => {
      this.manageAppLoaded()
    })

    return
    firebaseDatabase.init()
    FCM.manageNotification(this.props.navigation)
    if(this.props.configuration.configuration.local.subscribed === null){
      this.manageSubscription()
    }
    appService.setFirstInit(false)  

  }
  manageAppLoaded(){
    if(this.isManagedAppLoaded){
      return;
    }
    this.isManagedAppLoaded = true
    this.listenerUpdateApp = EventRegister.addEventListener(firebaseConstants.MANAGE_UPDATE_APP, (value) => {
      this.manageUpdateApp(value)
    })
  }
  async manageUpdateApp(value){
    if(!this.props.configuration.configuration.local.datetime){
      this.props.updateLocalConfiguration({
        ...value
      })
    }else{
      var datetime = this.props.configuration.configuration.local.datetime;
      if(datetime !== value.datetime){
        Toast.show({
          text: "La información de noticias se está actualizando",
          buttonText: 'Aceptar',
          duration: 10000
        })
  
        this.manageSavedNews()
        EventRegister.emit(firebaseConstants.UPDATE_APP, null)
        await this.props.updateLocalConfiguration({
          ...value
        })
        
      }else{
        // Son iguales no requiere actualizacion
      }
    }

  }
  async manageSavedNews(){
    var ids = this.getSavedNewsIds();
    if(ids.length > 0){
      var url = appConfig.config.app.REVIEW_IDS_SERVICE + ids.join(',')
      const result = await JsonApi.getAll(url)
      var reviewIds = result.reviewIds;
      var deletedIds = _.filter(ids, (uid)=>{
        var index = _.findIndex(reviewIds, (reviewId)=>{
          return reviewId.uid == uid
        }) 
        return index === -1
      })
      this.props.savedNews_remove(deletedIds)
    }
  }
  getSavedNewsIds(){
    return Object.keys(this.props.savedNews.news);
  }

  componentWillUnmount() {
    this.isComponentMount = false
    EventRegister.removeEventListener(this.listenerAppLoaded)
    if(this.listenerUpdateApp){
      EventRegister.removeEventListener(this.listenerUpdateApp)
    }
  }    
  manageSubscription(){
    FCM.manageSubscription(!!this.props.configuration.configuration.local.subscribed).then(()=>{
      var subscribed = !this.props.configuration.configuration.local.subscribed
      this.props.updateLocalConfiguration({
        subscribed: !this.props.configuration.configuration.local.subscribed
      })
      Toast.show({
        text: subscribed? 'Notificaciones activadas':'Notificaciones desactivadas',
        buttonText: 'Aceptar',
        duration: 5000
      })
    }).catch((error)=>{
      if(this.props.configuration.configuration.local.subscribed === null){
        this.props.updateLocalConfiguration({
          subscribed: false
        })  
      }
      Toast.show({
        text: 'La suscripción no fue actualizada. Revise su conexión a internet',
        buttonText: 'Aceptar',
        duration: 5000
      })
    })

  }  
  render() {

    return null
    
  }
}


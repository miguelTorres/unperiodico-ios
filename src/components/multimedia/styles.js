import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import { responsiveFontSize, calculateResponsiveValue } from '../../helpers/Responsive';
export default {
  container: {
    backgroundColor: "#FFF"
  },
  foreground: {
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  listItem: {
    /*flex: 1,*/
    borderBottomColor: '#bdbdbd',

    borderBottomWidth: 0.5,
    paddingLeft: 0,
    marginLeft: 10,
    paddingRight: 0,
    marginRight: 10,
    /*alignItems: 'flex-end', 
    justifyContent: 'flex-end',*/
  },
  listItemBody: {
    flex: 1,
    borderColor: 'rgba(256, 256, 256, 0)'
  },
  mb: {
    marginBottom: 15
  },
  prominentItemContainer: {
    flex: 1,
    justifyContent: 'center',
    /*paddingBottom: 50,
    marginBottom: 50,*/
  },
  prominentCarouselContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#dd0'
  },
  figCaption: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(128, 24, 54, 0.8)',
    padding: calculateResponsiveValue(10, 50),
    paddingTop: calculateResponsiveValue(10, 24),
    paddingBottom: calculateResponsiveValue(10, 24),
    //paddingBottom: 0,

  },
  customCaptionStyle:{
    bottom: calculateResponsiveValue(20, 28),
  },
  figCaptionText: {
    color: '#fff',
    fontSize: calculateResponsiveValue(12, 20),
    lineHeight: calculateResponsiveValue(14, 22),
  },
  decoration: {
    position: 'absolute',
    top: calculateResponsiveValue(10, 40),
    left: calculateResponsiveValue(15, 35),

    flexDirection: 'row',
    flexWrap:'wrap'

  },
  decorationText: {

    padding: calculateResponsiveValue(2, 4),
    paddingLeft: calculateResponsiveValue(10, 10),
    paddingRight: calculateResponsiveValue(10, 10),
    marginLeft: calculateResponsiveValue(5),
    marginRight: calculateResponsiveValue(5),
    fontSize: calculateResponsiveValue(9, 13.5),

    marginRight: 0,
    opacity: 0.9
  },
  imageInCarousel: {
    width: '100%',
    height: '100%'
  },
  imageOutCarousel: {
    resizeMode: "contain",
    width: '100%',
    height: '30%',
    flex: 1
  },
  containerPreviewIcon: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    
    //backgroundColor: 'rgba(255, 255, 255, 0.4)',
  },
  previewIcon: {
    color: unperiodicoVariables.titleFontColor,
    fontSize: 75,
    opacity: 0.6
  },
};

import React from "react";
import {View, Text} from "react-native"
import { Root } from "native-base";
import { createStackNavigator, createDrawerNavigator, createSwitchNavigator, createAppContainer } from "react-navigation";


import Loading from "../screens/loading"
import Items from "../screens/Items/Items";
import News from "../screens/News/News";
import NewsItem from "../screens/NewsItem/NewsItem";
import Home from "../screens/home";
import Detail from "../screens/detail";
import Author from "../screens/author";
import Poll from "../screens/poll";
import NewsContainer from "../screens/NewsContainer";
import SavedNews from "../screens/savedNews";
import AjaxPage from "../screens/ajaxPage";
import Searchbar from "../screens/searchbar/";


import Header from "../screens2/Header/";
import Header1 from "../screens2/Header/1";
import Header2 from "../screens2/Header/2";
import Header3 from "../screens2/Header/3";
import Header4 from "../screens2/Header/4";
import Header5 from "../screens2/Header/5";
import Header6 from "../screens2/Header/6";
import Header7 from "../screens2/Header/7";
import Header8 from "../screens2/Header/8";
import BasicFooter from "../screens2/footer/basicFooter";
import IconFooter from "../screens2/footer/iconFooter";
import IconText from "../screens2/footer/iconText";
import BadgeFooter from "../screens2/footer/badgeFooter";
import Default from "../screens2/button/default";
import Outline from "../screens2/button/outline";
import Rounded from "../screens2/button/rounded";
import Block from "../screens2/button/block";
import Full from "../screens2/button/full";
import Custom from "../screens2/button/custom";
import Transparent from "../screens2/button/transparent";
import IconBtn from "../screens2/button/iconBtn";
import Disabled from "../screens2/button/disabled";
import BasicCard from "../screens2/card/basic";
import NHCardImage from "../screens2/card/card-image";
import NHCardShowcase from "../screens2/card/card-showcase";
import NHCardList from "../screens2/card/card-list";
import NHCardHeaderAndFooter from "../screens2/card/card-header-and-footer";
import BasicFab from "../screens2/fab/basic";
import MultipleFab from "../screens2/fab/multiple";
import FixedLabel from "../screens2/form/fixedLabel";
import InlineLabel from "../screens2/form/inlineLabel";
import FloatingLabel from "../screens2/form/floatingLabel";
import PlaceholderLabel from "../screens2/form/placeholder";
import StackedLabel from "../screens2/form/stacked";
import RegularInput from "../screens2/form/regular";
import UnderlineInput from "../screens2/form/underline";
import RoundedInput from "../screens2/form/rounded";
import IconInput from "../screens2/form/iconInput";
import SuccessInput from "../screens2/form/success";
import ErrorInput from "../screens2/form/error";
import DisabledInput from "../screens2/form/disabledInput";
import RowNB from "../screens2/layout/row";
import ColumnNB from "../screens2/layout/column";
import NestedGrid from "../screens2/layout/nested";
import CustomRow from "../screens2/layout/customRow";
import CustomCol from "../screens2/layout/customCol";
import BasicListSwipe from "../screens2/listSwipe/basic-list-swipe";
import MultiListSwipe from "../screens2/listSwipe/multi-list-swipe";
import NHBasicList from "../screens2/list/basic-list";
import NHListDivider from "../screens2/list/list-divider";
import NHListSeparator from "../screens2/list/list-separator";
import NHListHeader from "../screens2/list/list-headers";
import NHListIcon from "../screens2/list/list-icon";
import NHListAvatar from "../screens2/list/list-avatar";
import NHListThumbnail from "../screens2/list/list-thumbnail";
import RegularPicker from "../screens2/picker/regularPicker";
import PlaceholderPicker from "../screens2/picker/placeholderPicker";
import PlaceholderPickerNote from "../screens2/picker/placeholderPickernote";
import BackButtonPicker from "../screens2/picker/backButtonPicker";
import HeaderPicker from "../screens2/picker/headerPicker";
import HeaderStylePicker from "../screens2/picker/headerStylePicker";
import CustomHeaderPicker from "../screens2/picker/customHeaderPicker";
import BasicTab from "../screens2/tab/basicTab";
import ConfigTab from "../screens2/tab/configTab";
import ScrollableTab from "../screens2/tab/scrollableTab";
import BasicSegment from "../screens2/segment/SegmentHeader";
import RegularActionSheet from "../screens2/actionsheet/regular";
import IconActionSheet from "../screens2/actionsheet/icon";
import AdvSegment from "../screens2/segment/segmentTab";
import SimpleDeck from "../screens2/deckswiper/simple";
import AdvancedDeck from "../screens2/deckswiper/advanced";

import Home2 from "../screens2/home/";
import Anatomy from "../screens2/anatomy/";
import Footer from "../screens2/footer/";
import NHBadge from "../screens2/badge/";
import NHButton from "../screens2/button/";
import NHCard from "../screens2/card/";
import NHCheckbox from "../screens2/checkbox/";
import NHDeckSwiper from "../screens2/deckswiper/";
import NHFab from "../screens2/fab/";
import NHForm from "../screens2/form/";
import TextArea from "../screens2/form/textArea";
import NHIcon from "../screens2/icon/";
import ListSwipe from "../screens2/listSwipe/";
import BasicIcon from "../screens2/icon/basic";
import IconState from "../screens2/icon/state";
import SpecificIcon from "../screens2/icon/specific";
import NHLayout from "../screens2/layout/";
import NHList from "../screens2/list/";
import NHRadio from "../screens2/radio/";
import NHSearchbar from "../screens2/searchbar/";
import NHSpinner from "../screens2/spinner/";
import NHPicker from "../screens2/picker/";
import NHTab from "../screens2/tab/";
import NHThumbnail from "../screens2/thumbnail/";
import NHTypography from "../screens2/typography/";
import SideBar from "../screens/sidebar";
import Segment from "../screens2/segment";
import Toast from "../screens2/toast";
import Actionsheet from "../screens2/actionsheet";

import FloatingView from "./FloatingView"
import TransversalActions from "./TransversalActions"
import Test from "./Test"
import connectComponentService from "../models/connectComponent";
import appService from "../services/appService";




const ItemsConnected = connectComponentService.createGeneralComponent(Items)
const SideBarConnected = connectComponentService.createGeneralComponent(SideBar)
const DetailConnected = connectComponentService.createGeneralComponent(Detail)
const AuthorConnected = connectComponentService.createGeneralComponent(Author)
const LoadingConnected = connectComponentService.createGeneralComponent(Loading)
const SavedNewsConnected = connectComponentService.createGeneralComponent(SavedNews)
const TransversalActionsConnected = connectComponentService.createGeneralComponent(TransversalActions)


const HomeStack = createStackNavigator({
  Home: { screen: Home },
  Poll: { screen: Poll },
  AjaxPage: { screen: AjaxPage },
  SavedNews: { screen: SavedNewsConnected },
  Searchbar: { screen: Searchbar },
  Detail: { screen: DetailConnected },
  Author: { screen: AuthorConnected },

  NewsContainer: {
		screen: NewsContainer
	},
}, {
  initialRouteName: 'Home',
  headerMode: 'none',
});


const Drawer = createDrawerNavigator(
  {
    HomeStack: { screen: HomeStack },
    Home2: { screen: Home2 },
    Items: { screen: ItemsConnected },
    Anatomy: { screen: Anatomy },
    Header: { screen: Header },
    Footer: { screen: Footer },
    NHBadge: { screen: NHBadge },
    NHButton: { screen: NHButton },
    NHCard: { screen: NHCard },
    NHCheckbox: { screen: NHCheckbox },
    NHDeckSwiper: { screen: NHDeckSwiper },
    NHFab: { screen: NHFab },
    NHForm: { screen: NHForm },
    NHIcon: { screen: NHIcon },
    BasicIcon: { screen: BasicIcon },
    IconState: { screen: IconState },
    SpecificIcon: { screen: SpecificIcon },
    NHLayout: { screen: NHLayout },
    NHList: { screen: NHList },
    ListSwipe: { screen: ListSwipe },
    NHRadio: { screen: NHRadio },
    NHSearchbar: { screen: NHSearchbar },
    NHSpinner: { screen: NHSpinner },
    NHPicker: { screen: NHPicker },
    NHTab: { screen: NHTab },
    NHThumbnail: { screen: NHThumbnail },
    NHTypography: { screen: NHTypography },
    Segment: { screen: Segment },
    Toast: { screen: Toast },
    Actionsheet: { screen: Actionsheet }
  },
  {
    initialRouteName: "HomeStack",
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    contentComponent: props => <SideBarConnected {...props} />
  }
);

const AppNavigator = createStackNavigator(
  {
    Drawer: { screen: Drawer },

    NewsItem: { screen: NewsItem },

    Header1: { screen: Header1 },
    Header2: { screen: Header2 },
    Header3: { screen: Header3 },
    Header4: { screen: Header4 },
    Header5: { screen: Header5 },
    Header6: { screen: Header6 },
    Header7: { screen: Header7 },
    Header8: { screen: Header8 },

    BasicFooter: { screen: BasicFooter },
    IconFooter: { screen: IconFooter },
    IconText: { screen: IconText },
    BadgeFooter: { screen: BadgeFooter },

    Default: { screen: Default },
    Outline: { screen: Outline },
    Rounded: { screen: Rounded },
    Block: { screen: Block },
    Full: { screen: Full },
    Custom: { screen: Custom },
    Transparent: { screen: Transparent },
    IconBtn: { screen: IconBtn },
    Disabled: { screen: Disabled },

    BasicCard: { screen: BasicCard },
    NHCardImage: { screen: NHCardImage },
    NHCardShowcase: { screen: NHCardShowcase },
    NHCardList: { screen: NHCardList },
    NHCardHeaderAndFooter: { screen: NHCardHeaderAndFooter },

    SimpleDeck: { screen: SimpleDeck },
    AdvancedDeck: { screen: AdvancedDeck },

    BasicFab: { screen: BasicFab },
    MultipleFab: { screen: MultipleFab },

    FixedLabel: { screen: FixedLabel },
    InlineLabel: { screen: InlineLabel },
    FloatingLabel: { screen: FloatingLabel },
    PlaceholderLabel: { screen: PlaceholderLabel },
    StackedLabel: { screen: StackedLabel },
    RegularInput: { screen: RegularInput },
    UnderlineInput: { screen: UnderlineInput },
    RoundedInput: { screen: RoundedInput },
    IconInput: { screen: IconInput },
    SuccessInput: { screen: SuccessInput },
    ErrorInput: { screen: ErrorInput },
    DisabledInput: { screen: DisabledInput },
    TextArea: { screen: TextArea },

    RowNB: { screen: RowNB },
    ColumnNB: { screen: ColumnNB },
    NestedGrid: { screen: NestedGrid },
    CustomRow: { screen: CustomRow },
    CustomCol: { screen: CustomCol },

    NHBasicList: { screen: NHBasicList },
    NHListDivider: { screen: NHListDivider },
    NHListSeparator: { screen: NHListSeparator },
    NHListHeader: { screen: NHListHeader },
    NHListIcon: { screen: NHListIcon },
    NHListAvatar: { screen: NHListAvatar },
    NHListThumbnail: { screen: NHListThumbnail },

    BasicListSwipe: { screen: BasicListSwipe },
    MultiListSwipe: { screen: MultiListSwipe },

    RegularPicker: { screen: RegularPicker },
    PlaceholderPicker: { screen: PlaceholderPicker },
    PlaceholderPickerNote: { screen: PlaceholderPickerNote },
    BackButtonPicker: { screen: BackButtonPicker },
    HeaderPicker: { screen: HeaderPicker },
    HeaderStylePicker: { screen: HeaderStylePicker },
    CustomHeaderPicker: { screen: CustomHeaderPicker },

    BasicTab: { screen: BasicTab },
    ConfigTab: { screen: ConfigTab },
    ScrollableTab: { screen: ScrollableTab },

    BasicSegment: { screen: BasicSegment },
    AdvSegment: { screen: AdvSegment },

    RegularActionSheet: { screen: RegularActionSheet },
    IconActionSheet: { screen: IconActionSheet }
  },
  {
    initialRouteName: "Drawer",
    headerMode: "none"
  }
);

var initialRouteName = appService.isFirstInit()? 'Loading' : 'App'

const SwitchApp = createSwitchNavigator(
  {
    Loading: {
      screen: LoadingConnected
    },
    App: {
      screen: AppNavigator
    },
  },
  {
    initialRouteName: initialRouteName
  }
);

const AppContainer = createAppContainer(SwitchApp);

class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}

const testComponent = <Test />
const appComponent = <App />
const SingleFloatingView = <FloatingView component={appComponent}><TransversalActionsConnected /></FloatingView>
export default () =>
  <Root>
    {SingleFloatingView}
  </Root>;

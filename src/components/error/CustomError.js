import React, { Component } from 'react'
import {
  View,
  StyleSheet,
} from "react-native";
import {
  Icon,
  Button,
  Text
} from "native-base";



const styles = StyleSheet.create({
  buttonContainer: {
    justifyContent: 'center',
    flexDirection: "row",
  }
});


export default class CustomError extends Component {


  render() {

    return(
      <View>
        <View><Text>{this.props.error}</Text></View>
        <View style={styles.buttonContainer}>
          <Button primary onPress={()=>{this.props.onPress()}}>
            <Icon active name={this.props.icon.name} type={this.props.icon.type} />
            <Text>{this.props.buttonText}</Text>
          </Button>
        </View>
      </View>
    )


  }
}


export default {
  content: {
    position: 'absolute',
    padding: 30,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(54, 54, 54, 0.5)',
  },
  container: {
    backgroundColor: "#FFF",
  },
  decoration: {
    position: 'absolute',
    top: 10,
    left: 10,
    flexDirection: 'row',
    flexWrap:'wrap'

  },
  decorationText: {
    padding: 2,
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 5,
    marginRight: 5,
    fontSize: 9,
    marginRight: 0,
    opacity: 0.9
  },

  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  listItem: {
    /*flex: 1,*/
    //borderBottomColor: '#bdbdbd',

    borderBottomWidth: 0,
    paddingLeft: 0,
    marginLeft: 0,
    paddingRight: 0,
    marginRight: 0,
    /*alignItems: 'flex-end', 
    justifyContent: 'flex-end',*/
  },
  listItemBody: {
    flex: 1,
    borderBottomWidth: 0,
    //borderColor: 'red'
    //borderColor: 'rgba(256, 256, 256, 0)'
  },
  mb: {
    marginBottom: 15,
    padding: 10
  },
  prominentItemContainer: {
    flex: 1,
    justifyContent: 'center',
    /*paddingBottom: 50,
    marginBottom: 50,*/
  },
  prominentCarouselContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#dd0'
  },
};

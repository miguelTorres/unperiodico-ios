import React, { PureComponent } from 'react'
import {
  View,
  Image,
  ImageBackground,
  TouchableOpacity
} from "react-native";
import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  H3,
  Card,
  CardItem
} from "native-base";
import HTML from '../../../HTML';
import moment from 'moment'
require('moment/locale/es');

import config from '../../../../../config'
import styleVariables from '../../../../../native-base-theme/variables/unperiodico-material'
import styles from "./styles"
import appConfig from '../../../../models/appConfig';
import Assets from '../../../../../assets'
import configurationHelper from '../../../../helpers/ConfigurationHelper'
import MediaSelectorHelper from '../../../../helpers/MediaSelectorHelper'

import avoidDoubleAction from '../../../../services/avoidDoubleAction'
export default class ImageBackgroundItem extends PureComponent {


  render() {
    const item = this.props.data

    const mediaSelectorHelper = new MediaSelectorHelper(item, true)
    //const selector = new AudioSelectorHelper(item)
    const selector = mediaSelectorHelper.getSelector()
    this.selector = selector
    const img = selector.getImageUrl(0)




    const categories = item.additional.categories
    const NEWS_TYPE = configurationHelper.getConfiguration(this.props.NEWS_TYPE, categories)
    const tagNewsType = NEWS_TYPE && NEWS_TYPE.tag!=''? NEWS_TYPE.tag:null
    const CONTENT_TYPE = configurationHelper.getConfiguration(this.props.CONTENT_TYPE, categories)
    const tagContentType = CONTENT_TYPE && CONTENT_TYPE.tag!=''? CONTENT_TYPE.tag:null


    const title = '<h1>'+item.title+'</h1>'
    const teaser = item.additional.teaser
    const date = moment(item.datetime).format(appConfig.config.app.SONDEOS.formatDate).toUpperCase()
    const seat = (!!item.additional.seat)? ' | '+item.additional.seat.toUpperCase() : ''
    const dateSeat = '<p class="date">'+date + seat+'</p>'
    const mainCategory = (!!item.additional.mainCategory && false)? '<category>'+item.additional.mainCategory.toUpperCase()+'</category>' : null

    const author = item.additional.author? {
      name:  
      '<div class="main-author"><p class="author">'+item.additional.author.name+'</p></div>'
      ,
      title: item.additional.author.title,
      firstName: item.additional.author.firstName,
      lastName: item.additional.author.lastName,
    }:null
    const authorHtml = author? '<div class="main-author"><p class="author">'+author.firstName+'</p></div>':
    ''

    return (
      <ListItem thumbnail style={styles.listItem} >
                <View style={styles.listItemBody}>
                <ImageBackground source={{uri:img}} style={{width: '100%', height: '100%'}}>
               
                </ImageBackground>                
                <View style={styles.content}>
                <TouchableOpacity onPress={() => {
                      if(!avoidDoubleAction.isActionActive('detail')){
                        return
                      }
                      this.props.navigation.navigate ('Detail', {
                        item
                      });
                    }} >                  
                  {mainCategory && 
                          <HTML stylesConfiguration='carouselConfiguration' html={mainCategory} />
                        }
                        <HTML stylesConfiguration='carouselConfiguration' html={dateSeat} />
                        {author && 
                          <HTML stylesConfiguration='listAuthorConfiguration' html={authorHtml} />
                        }
                        <HTML stylesConfiguration='carouselConfiguration' html={title} />
                        <HTML 
                          stylesConfiguration='carouselConfiguration'
                          html={teaser}
                        />
                  </TouchableOpacity>
                
                  </View>   
                  <View style={styles.decoration}>
                  {tagNewsType !== null && tagNewsType.name!="" &&
                    <Text style={[styles.decorationText, {color: tagNewsType.color, backgroundColor:tagNewsType.bgcolor}]}>{tagNewsType.name}</Text>
                  }
                  {tagContentType !== null && tagContentType.name!="" &&
                    <Text style={[styles.decorationText, {color: tagContentType.color, backgroundColor:tagContentType.bgcolor}]}>{tagContentType.name}</Text>
                  }
                  </View>

                </View>
              </ListItem>
    )

    


  }
}

import React, { PureComponent } from 'react'
import {
  View,
  Image,
  ImageBackground,
  TouchableOpacity
} from "react-native";
import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  H3,
  Card,
  CardItem
} from "native-base";
import HTML from '../../../HTML';
import moment from 'moment'
require('moment/locale/es');
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import config from '../../../../../config'
import styleVariables from '../../../../../native-base-theme/variables/unperiodico-material'
import styles from "./styles"
import appConfig from '../../../../models/appConfig';
import Assets from '../../../../../assets'
import configurationHelper from '../../../../helpers/ConfigurationHelper'
import MediaSelectorHelper from '../../../../helpers/MediaSelectorHelper'
import avoidDoubleAction from '../../../../services/avoidDoubleAction';
import { calculateResponsiveValue } from '../../../../helpers/Responsive';


export default class RecientesCarouselViewItem extends PureComponent {


  render() {
    const item = this.props.data

    const mediaSelectorHelper = new MediaSelectorHelper(item, true)
    //const selector = new AudioSelectorHelper(item)
    const selector = mediaSelectorHelper.getSelector()
    this.selector = selector
    const img = selector.getImageUrl(0)




    const categories = item.additional.categories
    const NEWS_TYPE = configurationHelper.getConfiguration(this.props.NEWS_TYPE, categories)
    const tagNewsType = NEWS_TYPE && NEWS_TYPE.tag!=''? NEWS_TYPE.tag:null
    const CONTENT_TYPE = configurationHelper.getConfiguration(this.props.CONTENT_TYPE, categories)
    const tagContentType = CONTENT_TYPE && CONTENT_TYPE.tag!=''? CONTENT_TYPE.tag:null



    const title = '<h1>'+item.title+'</h1>'
    const teaser = item.additional.teaser
    const date = moment(item.datetime).format(appConfig.config.app.SONDEOS.formatDate).toUpperCase()
    const seat = (!!item.additional.seat)? ' | '+item.additional.seat.toUpperCase() : ''
    const dateSeat = '<p class="date">'+date + seat+'</p>'
    const mainCategory = (!!item.additional.mainCategory)? '<div class="wraper-category"><p class="category">'+item.additional.mainCategory.toUpperCase()+'</p></div>'  : null

    
    return (
      <ListItem thumbnail style={styles.listItem} >
                <View style={styles.listItemBody}>
                <View style={{backgroundColor: 'black'}}>
                  <Image
                      style={{
                        resizeMode: "contain",
                        width: '100%',
                        height: calculateResponsiveValue(200, 400),
                      }}
                      source={{uri:img}}
                    />
                </View>
                <View style={styles.content}>
                  <View style={styles.innerText}>
                  {mainCategory && 
                          <HTML stylesConfiguration='recientesConfiguration' html={mainCategory} />
                        }
                        <HTML stylesConfiguration='recientesConfiguration' html={dateSeat} />

                <TouchableOpacity onPress={() => {
                      if(!avoidDoubleAction.isActionActive('detail')){
                        return
                      }
                      this.props.navigation.navigate ('Detail', {
                        item
                      });
                    }} >                  
                        <HTML stylesConfiguration='recientesConfiguration' html={title} />
                        </TouchableOpacity>
                        <HTML 
                          stylesConfiguration='recientesConfiguration'
                          html={teaser}
                        />
                        </View>
                  
                
                  </View>   
                  <View style={styles.decoration}>
                  {tagNewsType !== null && tagNewsType.name!="" &&
                    <Text style={[styles.decorationText, {color: tagNewsType.color, fontWeight: 'bold', backgroundColor:tagNewsType.bgcolor}]}>{tagNewsType.name}</Text>
                  }
                  {tagContentType !== null && tagContentType.name!="" &&
                    <Text style={[styles.decorationText, {color: tagContentType.color,fontWeight: 'bold',  backgroundColor:tagContentType.bgcolor}]}>{tagContentType.name}</Text>
                  }
                  </View>

                </View>
              </ListItem>
    )

    


  }
}

import { responsiveFontSize, calculateResponsiveValue } from "../../../../helpers/Responsive";

export default {
  content: {
    padding: 0,
  },
  container: {
    backgroundColor: "#FFF",
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  decoration: {
    position: 'absolute',
    top: calculateResponsiveValue(10, 30),
    left: calculateResponsiveValue(15, 69),

    flexDirection: 'row',
    flexWrap:'wrap'

  },
  decorationText: {


    padding: calculateResponsiveValue(2, 4),
    paddingLeft: calculateResponsiveValue(10, 10),
    paddingRight: calculateResponsiveValue(10, 10),
    marginLeft: calculateResponsiveValue(5),
    marginRight: calculateResponsiveValue(5),
    fontSize: calculateResponsiveValue(9, 13.5),

    marginRight: 0,
    opacity: 0.9
  },
  
  innerText: { //texto del carrusel
   
    padding: calculateResponsiveValue(20, 70),
    paddingTop: calculateResponsiveValue(15, 55),
    //justifyContent: 'flex-start',
    alignItems: 'flex-start',
    
  },

  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  listItem: {
    /*flex: 1,*/
    //borderBottomColor: '#bdbdbd',

    borderBottomWidth: 0,
    paddingLeft: 0,
    marginLeft: 0,
    paddingRight: 0,
    marginRight: 0,
   
  },
  listItemBody: {
    flex: 1,
    borderBottomWidth: 0,
  },
  mb: {
    marginBottom: 15,
    padding: 10
  },
  prominentItemContainer: {
    flex: 1,
    justifyContent: 'center',
    /*paddingBottom: 50,
    marginBottom: 50,*/
  },
  prominentCarouselContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#dd0'
  },
};

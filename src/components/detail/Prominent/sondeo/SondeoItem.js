
import React, { PureComponent } from 'react'
import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  H3,
  Card,
  CardItem,
  H1
} from "native-base";
import {
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  Alert,
  FlatList,
} from "react-native";
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import _ from 'lodash'
import moment from 'moment'
require('moment/locale/es');
import HTML from '../../../HTML'


import config from '../../../../../config'
import styleVariables from '../../../../../native-base-theme/variables/unperiodico-material'
import styles from "./styles"
import Assets from '../../../../../assets'
import appConfig from '../../../../models/appConfig'
import { calculateResponsiveValue } from '../../../../helpers/Responsive';

export default class SondeoItem extends PureComponent {

constructor(props){
  super(props)
  this.renderAnswerByPair = this.renderAnswerByPair.bind(this)
}
getAnswersByPair(){

  if(typeof this.answersByPair !== 'undefined'){
    return this.answersByPair
  }
  const item = this.props.data.item
  var answers = _(item.answers).mapValues((answer)=>{
    return {...answer}
  })
  .values()
  .value()

  this.total= _.reduce(answers, (sum, answer)=>{
    return sum + answer.counter;
  }, 0)


  this.answersByPair = _(answers).reduce(function(result, value, index, array) {
    if (index % 2 === 0)
      result.push(array.slice(index, index + 2));
    return result;
  }, []);
  //simular más respuestas
  //this.answersByPair.push(this.answersByPair[0])

  this.answersByPair = _.map(this.answersByPair, (pair, index)=>{
    return {
      pair: pair,
      key: ''+index
    }
  })
  return this.answersByPair

}
renderAnswer(answer, isLeft){
  const counterText = answer.counter === 1? 'voto' : 'votos'
  const percentage = Math.round(this.total === 0? 0 : answer.counter * 100 / this.total)
  return (
    <View style={styles.answerContainer}>
    <View>
    <AnimatedCircularProgress
      rotation={0}
      size={calculateResponsiveValue(120, 90)}
      width={calculateResponsiveValue(15, 10)}
      fill={percentage}
      tintColor={config.colors.config1.background}
      backgroundColor="rgba(198,198,198,1)" />
      <View style={styles.answerLabel}><Text>{percentage}</Text></View>
      </View>
      <View style={styles.answerDetail}>
        <View><Text style={styles.answerTitle}>{answer.title}</Text></View>
        <View><Text style={styles.answerCounter}>{answer.counter} {counterText}</Text></View>
      </View>
      </View>
  )
}
renderAnswerByPair({item}){
  const pair = item.pair
  const answer1 = this.renderAnswer(pair[0], true)
  const answer2 = pair.length>0 ? this.renderAnswer(pair[1], false) : null


  var {width} = Dimensions.get('window');
  width = width/2

  var height = 3 *width / 4 


  return (
    <View style={{flexDirection: 'row'}}>
      <View style={{alignItems: 'center', padding: 0}}>
        {answer1}
      </View>
      <View style={{alignItems: 'center',  padding: 0}}>
        {answer2}
      </View>
    </View>
  )
}
render() {
  const item = this.props.data.item
  const date = moment(item.datetime).format(appConfig.config.app.SONDEOS.formatDate).toUpperCase();
  //const date = appConfig.config.app.SONDEOS.formatDate
  const answersByPair = this.getAnswersByPair()

  return (
    <View style={styles.container}>
        
            <View style={styles.dateContainer}>
              <Text style={styles.date}>{date}</Text>
            </View>
            <H1 style={styles.title}>{item.question}</H1>
            <View style={{width: '100%', justifyContent: 'center', alignItems: 'center'}}>
            <FlatList
              contentContainerStyle={styles.circlesContainer}
              data={answersByPair}
              renderItem={this.renderAnswerByPair}
            />
            </View>
            {item.description?
              (<HTML stylesConfiguration='detailConfiguration' html={item.description} tagsStyles = {{ p: { fontSize: calculateResponsiveValue(14) } }} />)
              : null
            }
        
      </View> 
  )

  return (
    <Card style={styles.container}>
        <CardItem>
          <Body>
            <View style={styles.dateContainer}>
              <Text style={styles.date}>{date}</Text>
            </View>
            <H1 style={styles.title}>{item.question}</H1>
            <View style={{width: '100%', justifyContent: 'center', alignItems: 'center'}}>
            <FlatList
              contentContainerStyle={styles.circlesContainer}
              data={answersByPair}
              renderItem={this.renderAnswerByPair}
            />
            </View>
            {item.description?
              (<HTML stylesConfiguration='detailConfiguration' html={item.description} />)
              
              : null
            }
          </Body>
        </CardItem>
      </Card> 
  )



}
}

import config from '../../../../../config'
import { calculateResponsiveValue } from '../../../../helpers/Responsive';
export default {
  container: {
    marginTop: calculateResponsiveValue(10),
    marginBottom: calculateResponsiveValue(10),
    marginLeft: 0,
    marginRight: 0,
    paddingTop: calculateResponsiveValue(5),
    paddingBottom: calculateResponsiveValue(5),
    paddingLeft: calculateResponsiveValue(30, 50),
    paddingRight: calculateResponsiveValue(30, 50),
  },
  title: {
    color: config.colors.config2.color
  },
  dateContainer: {
    marginTop: calculateResponsiveValue(10),
    marginBottom: calculateResponsiveValue(10)
  },
  date: {
    fontSize: calculateResponsiveValue(12),
    fontWeight: 'bold'
  },
  circlesContainer: {
    marginTop: calculateResponsiveValue(20, 35),
    marginBottom: calculateResponsiveValue(20, 35),
  },
  points: {
    backgroundColor: 'transparent',
    position: 'absolute',
    top: 72,
    left: 56,
    width: 90,
    textAlign: 'center',
    color: '#7591af',
    fontSize: 50,
    fontWeight: "100"
  },
  answerContainer: {
    padding: 5,
    margin: 10,
  },
  answerLabel: {
    position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center'
  },
  answerDetail: {
    justifyContent: 'center', alignItems: 'center'
  },

  answerTitle: {
    fontSize: calculateResponsiveValue(18, 13),
    fontWeight: 'bold'
  },
  answerCounter: {
    fontSize: calculateResponsiveValue(12, 9),
  },

};

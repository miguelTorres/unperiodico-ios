import unperiodicoVariables from '../../../../../native-base-theme/variables/unperiodico-material'
export default {
  container: {
    backgroundColor: "#FFF"
  },
  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  listItem: {
    /*flex: 1,*/
    borderBottomColor: '#bdbdbd',

    borderBottomWidth: 0.5,
    paddingLeft: 0,
    marginLeft: 0,
    paddingRight: 0,
    marginRight: 0,
    /*alignItems: 'flex-end', 
    justifyContent: 'flex-end',*/
  },
  listItemBody: {
    flex: 1,
    borderColor: 'rgba(256, 256, 256, 0)'
  },
  mb: {
    marginBottom: 15
  },
  prominentItemContainer: {
    flex: 1,
    justifyContent: 'center',
    /*paddingBottom: 50,
    marginBottom: 50,*/
  },
  prominentCarouselContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#dd0'
  },
  decoration: {
    position: 'absolute',
    bottom: 10,
    left: 10,
  },
  decorationText: {
    padding: 5,
    fontSize: 10,
    marginTop: 3
  },
  imageInCarousel: {
    width: '100%',
    height: '100%'
  },
  imageOutCarousel: {
    resizeMode: "cover",
    width: '100%',
    height: 200,
    flex: 1
  },
  containerPreviewIcon: {
    position: 'absolute',
    top: 2,
    left: 13,
    padding: 5,
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
  },
  previewIcon: {
    color: unperiodicoVariables.titleFontColor
  },
};

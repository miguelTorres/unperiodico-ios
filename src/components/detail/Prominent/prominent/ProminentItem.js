
import React, { PureComponent } from 'react'
import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Icon,
  Body,
  H3,
} from "native-base";
import {
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  Alert
} from "react-native";
import _ from 'underscore'

import moment from 'moment'
require('moment/locale/es');

import HTML from '../../../HTML';
import styleVariables from '../../../../../native-base-theme/variables/unperiodico-material'
import styles from "./styles"
import Multimedia from '../../../multimedia/Multimedia';
import RelatedNews from '../../../relatednews/RelatedNews';
import tabService from '../../../../services/tabService';
import appConfig from '../../../../models/appConfig'
import Assets from '../../../../../assets'
import avoidDoubleAction from '../../../../services/avoidDoubleAction';
export default class ProminentItem extends PureComponent {

  constructor(props) {
    super(props)
    this.onPressItem = this.onPressItem.bind(this)
  }
  onPressItem(item){
    if(!avoidDoubleAction.isActionActive('detail')){
      return
    }
    const special = this.props.data.item
    this.props.navigation.navigate ('Detail', {
      item,
      related: special.additional.related,
      special
    });
  }

  renderRelated(item){
    if(item.additional.related && item.additional.related.length > 0){
      return (
        <RelatedNews title='Noticias en este especial' related={item.additional.related} navigation={this.props.navigation} onPressItem={this.onPressItem} />
      )  
    }
    return null
  }

 
  render() {
    const item = this.props.data.item
    
    const mediaPreview = <Multimedia {...this.props} item={item} />
    //const mediaPreview = null
    const title = '<h1>'+item.title+'</h1>'
    const teaser = item.additional.teaser
    const date = moment(item.datetime).format(appConfig.config.app.SONDEOS.formatDate).toUpperCase()
    const seat = (!!item.additional.seat)? ' | '+item.additional.seat.toUpperCase() : ''
    const dateSeat = '<date>'+date + seat+'</date>'
    const mainCategory = (!!item.additional.mainCategory && false)? '<category>'+item.additional.mainCategory.toUpperCase()+'</category>' : null

    const author = item.additional.author? {
      name:  
      '<div class="main-author"><p class="author">'+item.additional.author.name+'</p></div>'
      ,
      title: item.additional.author.title,
      firstName: item.additional.author.firstName,
      lastName: item.additional.author.lastName,
    }:null
    const authorHtml = author? '<div class="main-author"><p class="author">'+author.firstName+'</p></div>':
    ''

    return (
      
      <ListItem style={styles.listItem} >
                <Body style={styles.listItemBody}>
                  {mediaPreview}
                  {this.renderRelated(item)}

                      <View style={styles.content}>

                        {mainCategory && 
                          <HTML stylesConfiguration='homeConfiguration' html={mainCategory} />
                        }
                        <HTML stylesConfiguration='homeConfiguration' html={dateSeat} />
                        {author && 
                          <HTML stylesConfiguration='listAuthorConfiguration' html={authorHtml} />
                        }
                        <HTML stylesConfiguration='homeConfiguration' html={title} />
                        <HTML 
                          stylesConfiguration='homeConfiguration'
                          html={teaser}
                        />

                        <Button small transparent onPress={() => {
                          if(!avoidDoubleAction.isActionActive('detail')){
                            return
                          }
                          this.props.navigation.navigate ('Detail', {
                            item
                          });
                        }}>
                          <Text style={{paddingRight: 5, paddingLeft: 0, fontSize: 16, fontWeight: 'bold'}}>Ver</Text>       
                          <Image
                            style={{width: 20, height: 20, resizeMode: 'contain'}}
                            source={Assets.mas}
                          />
                        </Button>
                      </View>
                  
                </Body>
              </ListItem>
    )
  }
}

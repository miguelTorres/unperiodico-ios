import React, { PureComponent } from 'react'
import {
  View,
  TouchableOpacity,
} from "react-native";
import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  H3,
  CardItem,
  Card
} from "native-base";
import HTML from '../../HTML';

import config from '../../../../config'
import styleVariables from '../../../../native-base-theme/variables/unperiodico-material'
import styles from "../styles"
import appConfig from '../../../models/appConfig';
import tabService from '../../../services/tabService';
import MediaSelectorHelper from '../../../helpers/MediaSelectorHelper';
import { calculateResponsiveValue } from '../../../helpers/Responsive';
export default class RelatedPairItem extends PureComponent {

  calculateImage(item){
    const mediaSelectorHelper = new MediaSelectorHelper(item, true)
    //const selector = new AudioSelectorHelper(item)
    const selector = mediaSelectorHelper.getSelector()
    this.selector = selector
    const img = selector.getImageUrl(0)
    return img
  }
  renderItem(item, isAbove){
    if(typeof item === 'undefined'){
      return null
    }
    const img = typeof item.image !== 'undefined'? 
    appConfig.config.app.SITE_URL + item.image.src:
    this.calculateImage(item)

    const cardStyle = isAbove? styles.aboveCard : styles.belowCard
    const title = '<p>'+item.title.trim()+'</p>'
    return(
      <View style={cardStyle}>
      <Card style={{padding: 0, paddingTop: 0, }}>
        <CardItem style={styles.cardItemStyle} bordered> 
          <Left>
            <TouchableOpacity onPress={() => {
                      this.props.onPressItem(item);
            }} >
            <Thumbnail square source={{uri: img}} style={{height: calculateResponsiveValue(56, 75), width: calculateResponsiveValue(56, 75)}} />
            </TouchableOpacity>
            <Body>
            <TouchableOpacity onPress={() => {
              this.props.onPressItem(item);
                    }} >
              <View style={{paddingRight: calculateResponsiveValue(10)}}>
                <HTML stylesConfiguration='relatedConfiguration' html={title}  />
              </View>
            </TouchableOpacity>
            </Body>
          </Left>
        </CardItem>
      </Card> 
      </View>
    )
     
  }
  render() {
    const pair = this.props.data
    const item1 = this.renderItem(pair[0], true)
    const item2 = pair.length>0 ? this.renderItem(pair[1], false) : null

    return (
      <View style={{height: config.carouselRelated.height}}>
        <View style={{height: calculateResponsiveValue(80), padding: 0}}>
          {item1}
                
        </View>
        <View style={{height: calculateResponsiveValue(80), padding: 0}}>
          {item2}
        </View>
      </View>
    )
    


  }
}

import React, { Component } from 'react'

import HTML from 'react-native-render-html';
import { View } from 'native-base';
import { StyleSheet } from 'react-native';
import AjaxModel from '../models/ajax'
import CustomError from './error/CustomError'


export default class HTMLRender extends Component {
  constructor(props) {
    super(props)
    this.state = {
      refreshing: false,
      html: null,
    }
    this.renderers = {
      input: (attrs, children, convertedCSSStyles, passProps) => this.renderInput(attrs, children, convertedCSSStyles, passProps),
    }
    this.elements = []
    this.loadedForm = false

    this.alterData = this.alterData.bind(this)
  }
  componentDidMount = async () => {
    this.isComponentMount = true
    this.load()
  }
  componentWillUnmount() 
  {
    this.isComponentMount = false
  }  
  renderInput(attrs, children, convertedCSSStyles, passProps){
    if(typeof attrs.name !== 'undefined'){
      var element = {
        name: attrs.name,
      }
      if(typeof attrs.value !== 'undefined'){
        element.value = attrs.value
      }
  
      this.elements.push(element)
    }

    if(attrs.type == 'submit'){
      this.loadedForm = true
      setTimeout(() => {
        this.props.onLoadForm(this.elements)        
      }, 300);
    }
    return null
  }

  load = async () => {
    try {
      const { navigation } = this.props;
      const url = this.props.url;
        this.isComponentMount && this.setState({
          refreshing: true,
          error: null,
        });  
      const html = await AjaxModel.load(url).then((html)=>{
        this.isComponentMount && this.setState({
          refreshing: false,
          html: html,
        })
      }).catch((error)=>{
        this.isComponentMount && this.setState({
          refreshing: false,
          error: 'La funcionalidad está deshabilitada. Revise la conexión. Si persiste contacte con el administrador.',
        });
  
      });

    } catch (error) {
      throw error
    }
  };
  alterData = (node) => {
    let { parent, data } = node;
    if (parent && parent.name === 'input') {
        // Texts elements are always children of wrappers, this is why we check the tag
        // with "parent.name" and not "name"
        return data;
    }
    // Don't return anything (eg a falsy value) for anything else than the <h1> tag so nothing is altered
  }

  renderError(){
    return(
      <CustomError 
        error={this.state.error} 
        buttonText="Recargar" 
        onPress={()=>{this.load()}}
        icon = {{name: 'ios-refresh'}} 
        />
    )
  }
  renderBody(){
    if(!this.state.html){
      return null
    }

    return (
      <View style={styles.container}>
      <HTML ignoredTags={[]} html={this.state.html} renderers={this.renderers} alterData={this.alterData} {...this.props} />
      </View>
    )
  }
  render() {

    if(this.loadedForm){
      return null
    }
    const body = this.renderBody()
    const isError = !!this.state.error

    return(
      <View>
        {isError && this.renderError()} 
        {!isError && body}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: 0,
  },
});
import offline from 'react-native-simple-store'

export default function(store) {
  let currentData

  store.subscribe(() => {
    const { offlineLoaded, news } = store.getState().news
    if (offlineLoaded && currentData != news) {
      offline.save('news', news)
      currentData = news
    }
  })
}

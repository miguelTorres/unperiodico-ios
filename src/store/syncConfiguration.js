import offline from 'react-native-simple-store'

export default function(store) {
  let currentData

  store.subscribe(() => {
    const { offlineLoaded, configuration } = store.getState().configuration
    if (offlineLoaded && currentData != configuration) {
      offline.save('configuration', configuration)
      currentData = configuration
    }
  })
}

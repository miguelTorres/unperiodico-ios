import offline from 'react-native-simple-store'

export default function(store) {
  let currentData

  store.subscribe(() => {
    const { offlineLoaded, news } = store.getState().newsItem
    if (offlineLoaded && currentData != news) {
      offline.save('newsItem', news)
      currentData = news
    }
  })
}

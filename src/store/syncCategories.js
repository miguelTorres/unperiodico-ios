import offline from 'react-native-simple-store'

export default function(store) {
  let currentData

  store.subscribe(() => {
    const { offlineLoaded, categories } = store.getState().categories

    if (offlineLoaded && currentData != categories) {
      offline.save('categories', categories)
      currentData = categories
    }
  })
}

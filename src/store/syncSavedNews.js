import offline from 'react-native-simple-store'

export default function(store) {
  let currentData

  store.subscribe(() => {
    const { offlineLoaded, news } = store.getState().savedNews

    if (offlineLoaded && currentData != news) {
      offline.save('savedNews', news)
      currentData = news
    }
  })
}

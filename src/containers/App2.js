/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
 import React from "react";
 import Setup from "../boot/setup";

 type Props = {};
 export default class App extends React.Component<Props> {
   render() {
     return <Setup />;
   }
 }

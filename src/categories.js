import { loadCategories } from './actions/categories'




export function syncCategories(store) {
  store.dispatch(loadCategories())
}

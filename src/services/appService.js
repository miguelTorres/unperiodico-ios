
class AppService{
  constructor(){
    this.firstInit = true
  }
  isFirstInit(){
    return this.firstInit
  }
  setFirstInit(firstInit){
    this.firstInit = firstInit
  }
}
let appService = new AppService();
export default appService;
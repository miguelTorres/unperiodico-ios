
class AvoidDoubleAction{
  constructor(){
    this.timeout = 0
    this.action = {}
    this.isBusy = false;
    this.categoryTimeout = {
      detail: 800,
    }
    this.timer = null;
  }
  isActionActive(categoryTimeout){
    var finaltimeout = (!!categoryTimeout) && (!!this.categoryTimeout[categoryTimeout])? this.categoryTimeout[categoryTimeout] : this.timeout
    if(finaltimeout === 0){
      return true;
    }
    if(!this.isBusy){
      
      this.isBusy = true;
      this.timer = setTimeout(() => {
        this.isBusy = false;
        this.timer = null
      }, finaltimeout);
      return true;
    }
    return false
  }
  disableBusy(){
    if(this.timer){
      clearTimeout(this.timer)
      this.isBusy = false;
      this.timer = null
    }
  }
  isActionActiveWithTag(tag){
    if(typeof this.action[tag] === 'undefined'){
      this.action[tag] = true;
      setTimeout(() => {
        delete this.action[tag];
      }, this.timeout);
      return true;
    }
    return false
  }
}
let avoidDoubleAction = new AvoidDoubleAction();
export default avoidDoubleAction;
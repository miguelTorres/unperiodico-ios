class VariableHeadingService{
  constructor(){
    this.title = {}
  }
  setTitle(category, title){
    this.title[category] = title
  }
  getTitle(category){
    return this.title[category]
  }
}
let variableHeadingService = new VariableHeadingService();
export default variableHeadingService;
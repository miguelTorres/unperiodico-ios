import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import { responsiveFontSize, calculateResponsiveValue } from '../../helpers/Responsive';
import config from '../../../config'
export default {
  container: {
    backgroundColor: "#FFF"
  },
  tabsText: {
    fontSize: 13,
    paddingLeft: 2,
    paddingRight: 2,
  },
  headerButton: {
    borderWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },
  headerIcon: {
    marginLeft: 1,
    marginRight: calculateResponsiveValue(10, 30),
    fontSize: calculateResponsiveValue(25, 30),
  },
  tabStyleFooterTab: {
    backgroundColor: '#fff',

  },
  activeTabStyleFooterTab: {
color: '#801836'
  },
  textStyleFooterTab: {
    fontSize: calculateResponsiveValue(11, 15),
    color: '#801836'

  },
  activeTextStyleFooterTab: {
color: '#801836'
  },
  tabStyleHeaderTab: {
    borderWidth: 0,
    backgroundColor: '#4E4E4E',
  },
  activeTabStyleHeaderTab: {
    color: '#fff'

  },
  textStyleHeaderTab: {
    fontSize: calculateResponsiveValue(14, 21),
    fontWeight: 'bold',
    color: '#fff'
  },
  activeTextStyleHeaderTab: {
    color: '#F2D069',
  }
};


import React, { Component } from "react";
import { View, StatusBar, Text, FlatList, RefreshControl } from "react-native";
import { Container, Content, Header, Body, Left, Right, 
  StyleProvider, Title, H1
 } from "native-base";
 import _ from 'lodash'
 import moment from 'moment'
 require('moment/locale/es');
 
import Bar from '../bar/bar'
import DefaultItem from '../../components/detail/Prominent/default/DefaultItem'

import styles from "./styles";
import getTheme from '../../../native-base-theme/components'
import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import appConfig from '../../models/appConfig'
import FiltroFecha from "../../components/FiltroFecha";
import { EventRegister } from "react-native-event-listeners";
import firebaseConstants from "../../models/firebaseConstants";
import { timeout } from "../../helpers/Functions";
import CustomStatusBar from "../../components/CustomStatusBar";
import { calculateResponsiveValue } from "../../helpers/Responsive";
import CustomHeader from "../../components/CustomHeader";
import CustomRefreshControl from "../../components/CustomRefreshControl";


class SavedNews extends Component<{}, State> {
  constructor(props) {
    super(props);
    this.data =Object.keys(this.props.savedNews.news).map((key)=>{
      return this.props.savedNews.news[key]
    })
    this.state = {
      date: null,
      data: this.data,
      loading: true,
    }
    this.onDateChange=this.onDateChange.bind(this)
    this.resetDate=this.resetDate.bind(this)

  }
  reset(){
    this.setState({
      data: [],
      loading: true,
    })
    setTimeout(() => {
      this.data =Object.keys(this.props.savedNews.news).map((key)=>{
        return this.props.savedNews.news[key]
      })
      this.setState({
        date: null,
        data: this.data,
        loading: false,
      })
    }, 300);
  }
  async componentDidMount(){
    await timeout(300)
    this.setState({loading: false})
    this.listenerUpdateApp = EventRegister.addEventListener(firebaseConstants.UPDATE_APP, () => {
      this.reset()
    })
    this.isComponentMount = true
  }
  componentWillUnmount(){
    this.isComponentMount = false
    EventRegister.removeEventListener(this.listenerUpdateApp)

  }
  onDateChange(date){
    //moment(date)
    var timeRestriction =moment(date, 'YYYY-MM-DD')
    const data = _.filter(this.state.data, (item)=>{
      return timeRestriction.diff(moment(item.datetime))>0
    })

    this.isComponentMount && this.setState({
      date: date,
      data
    })
    
  }
  calculateData(){
    let data =Object.keys(this.props.savedNews.news).map((key)=>{
      return this.props.savedNews.news[key]
    })
    if(this.state.date){
      var timeRestriction =moment(this.state.date, 'YYYY-MM-DD')
      data = _.filter(this.state.data, (item)=>{
        return timeRestriction.diff(moment(item.datetime))>0
      })
    }
    return data;
  }
  resetDate(){
    this.isComponentMount && this.setState({
      date: null,
      data: this.data
    })

  }
  renderRow = (data) => {
    var NEWS_TYPE = appConfig.config.app.list.NEWS_TYPE
    var CONTENT_TYPE = appConfig.config.app.list.CONTENT_TYPE

    return (
      <DefaultItem {...this.props} saveNews={true} showMainCategory={1} data={data} NEWS_TYPE={NEWS_TYPE} CONTENT_TYPE={CONTENT_TYPE} />
    )

    return(
      <View><Text>Funcona</Text></View>

    )
  }
  render() {
    const { navigation } = this.props;
    const title = navigation.getParam('title', null);
    const data = this.calculateData()

    return (
        <StyleProvider style={getTheme(unperiodicoVariables)}>
        <Container>

          <CustomHeader>
          <CustomStatusBar />
            <Left>
              <Bar navigation={this.props.navigation} />
            </Left>
            <Body>
            </Body>
            <Right>
            </Right>
          </CustomHeader>
          <Content 
          refreshControl={
            <CustomRefreshControl
              refreshing={this.state.loading}
            />
          }
          >
          {data.length > 0 && !this.state.loading &&
            <FiltroFecha
              date={this.state.date}
              onDateChange={this.onDateChange}
              onResetDate={this.resetDate}
            />
            }

          <View style={styles.content}>
          {data.length == 0 && 
            <Text>No hay noticias guardadas.</Text>
            }
            {!this.state.loading && 
                        <FlatList
                        data={data}
                        renderItem={this.renderRow}
                        keyExtractor={item=>""+item.uid}
                        ItemSeparatorComponent={()=>null}
                        //refreshing={isRefreshing}
                        //onRefresh={this.props.handleRefresh}
                        //onEndReached={this.props.onEndReached}
                        
                               
                        />   
            }

            
          </View>

            
          </Content>
  
        </Container>
        </StyleProvider>
      );
  }

  
}

export default SavedNews

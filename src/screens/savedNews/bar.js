import React, { Component } from "react";
import { Image } from "react-native";
import { Icon, Button } from "native-base";

import styles from "./styles";


class Bar extends Component<{}, State> {

  render() {
    const { state } = this;

    return (
        <Button transparent style={styles.headerButton} onPress={() => this.props.navigation.goBack()}>
            <Icon style={styles.headerIcon} name="arrow-back" />
        </Button>
    );
  }
}

export default Bar;

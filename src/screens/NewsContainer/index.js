import React, { Component } from "react";
import { View, Text, StatusBar, WebView } from "react-native";
import { Container, Content, Header, Body, Left, Right, Title,
  StyleProvider,
 } from "native-base";
 

import Bar from '../bar/bar'

import styles from "./styles";
import getTheme from '../../../native-base-theme/components'
import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import appConfig from '../../models/appConfig'
import connectComponentService from "../../models/connectComponent";
import News from "../News/News";
import { calculateResponsiveValue } from "../../helpers/Responsive";
import CustomHeader from "../../components/CustomHeader";


class NewsContainer extends Component<{}, State> {
  constructor(props) {
    super(props);
  }
  componentWillMount(){
  }
  componentWillUnmount(){

  }

  createComponent(){
    if(typeof this.NewsConnected !== 'undefined'){
      return this.NewsConnected
    }
    const { navigation } = this.props;
    const url = navigation.getParam('url', null);
    const category = navigation.getParam('category', null);
    const jsonkey = navigation.getParam('jsonkey', null);
    const view = navigation.getParam('view', null);
    this.NewsConnected = connectComponentService.createNewsComponent(News, category)
    return this.NewsConnected
  }
  componentDidMount(){
    const { navigation } = this.props;
    const url = navigation.getParam('url', null);

  }

  render() {

    const { navigation } = this.props;
    const url = navigation.getParam('url', null);
    const category = navigation.getParam('category', null);
    const jsonkey = navigation.getParam('jsonkey', null);
    const view = navigation.getParam('view', null);
    const filtroFecha = navigation.getParam('filtroFecha', null);

    const title = navigation.getParam('title', null);
    const NewsConnected = this.createComponent()

    return (
        <StyleProvider style={getTheme(unperiodicoVariables)}>
        <Container  style={{flex: 1}}>



          <CustomHeader>
            <Left>
              <Bar navigation={this.props.navigation} />
            </Left>
            <Body>
            </Body>
            <Right>
            </Right>
          </CustomHeader>
          <Content contentContainerStyle={{ flex: 1 }} style={{flex: 1}}>
  
  
          <NewsConnected {...this.props} category = {category} url={url} jsonkey={jsonkey} view={view} filtroFecha={filtroFecha} />
         
            
          </Content>
  
        </Container>
        </StyleProvider>
      );
  }

  
}

export default NewsContainer

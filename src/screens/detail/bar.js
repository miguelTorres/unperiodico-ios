import React, { Component } from "react";
import { Image } from "react-native";
import { Icon, Button } from "native-base";

import styles from "./styles";
import tabService from "../../services/tabService";
import Assets from '../../../assets'
import { calculateResponsiveValue } from "../../helpers/Responsive";


class Bar extends Component<{}, State> {

  render() {
    const { state } = this;

    return (

        <Button bordered style={styles.headerButton} onPress={() => {
          tabService.disableService(300)
          this.props.navigation.goBack()}
          }>
            <Icon style={styles.headerIcon} name="arrow-back"  />
            <Image
        style={{height: calculateResponsiveValue(30, 45), width: calculateResponsiveValue(106, 159), }}
        resizeMode={'contain'}
        source={Assets.logo}>
        </Image>
        </Button>
    );
  }
}

export default Bar;

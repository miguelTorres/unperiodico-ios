import React, { Component } from "react";
import { StyleSheet, ListView, ImageBackground, View, StatusBar, Image, UIManager, Alert, RefreshControl, Modal, TouchableHighlight, TouchableOpacity, Clipboard, ToastAndroid, AlertIOS, Platform, Dimensions, Animated } from "react-native";
import { Container, Content, Header, Body, Left, Right, Icon, H3, H1, H2, Text, List, ListItem, Thumbnail, Button, Card, CardItem, Title, Grid, Col,   Footer,
  FooterTab,
  StyleProvider,
  Fab,
  IconNB,
  Toast
 } from "native-base";

 import moment from 'moment'
 require('moment/locale/es');

 
 import HTML from '../../components/HTML';
 import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils';
 import Share from 'react-native-share';

import Bar from './bar'
import CustomError from '../../components/error/CustomError'

import styles from "./styles";
import getTheme from '../../../native-base-theme/components'
import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import appConfig from '../../models/appConfig'
import NewsItemModel from '../../models/newsItem'
import Message from "../../components/Message";
import Multimedia from "../../components/multimedia/Multimedia";
import Assets from '../../../assets'

import firebaseAnalytics from "../../models/firebaseAnalytics";
import avoidDoubleAction from "../../services/avoidDoubleAction";
import newsHelper from "../../helpers/NewsHelper";

import firebaseConstants from "../../models/firebaseConstants";
import { EventRegister } from "react-native-event-listeners";
import { timeout } from "../../helpers/Functions";
import FCM from "../../models/fcm";
import { calculateResponsiveValue } from "../../helpers/Responsive";
import CustomHeader from "../../components/CustomHeader";
import CustomRefreshControl from "../../components/CustomRefreshControl";


class Author extends Component<{}, State> {
  constructor(props) {
    super(props);
    const { navigation } = this.props;
    const item = navigation.getParam('item', null);
    const uid = item.uid
    this.state = {
      active: false,
      isOnline: !!this.props.newsItem.online[uid],
      showSave: typeof this.props.newsItem.news[uid] !== 'undefined',
      anim: new Animated.Value(0)
    }

    
    this.openAuthor = this.openAuthor.bind(this)
    this.closeAuthor = this.closeAuthor.bind(this)
    this.setAuthorContentHeight = this.setAuthorContentHeight.bind(this)
    this.setContentWidth = this.setContentWidth.bind(this)
    
  }
  renderImg(attrs, children, convertedCSSStyles, passProps){
    const url = appConfig.config.app.SITE_URL + attrs.src
    return(
      <View key={passProps.key}>
      <Image
        style={styles.htmlImage}
        source={{uri: url}}
      />
      </View>
    )
  }
  getUid(){
    const { navigation } = this.props;
    const item = navigation.getParam('item', null);
    const uid = item.uid
    return uid
  }

   async componentDidMount() {
    //avoidDoubleAction.disableBusy()
    const uid = this.getUid()
    FCM.setCurrentUid(uid)
    this.listenerUpdateApp = EventRegister.addEventListener(firebaseConstants.UPDATE_APP, () => {
      this.makeRemoteRequest()
    })
    this.isComponentMount = true
    await timeout(300)
    this.setState({loading: false, anim: new Animated.Value(0)})
    
    if(!!this.props.newsItem.online[uid]){
      return;
    }
    this.makeRemoteRequest()
  }
  componentWillUnmount() {
    FCM.removeCurrentUid()
    this.isComponentMount = false
    EventRegister.removeEventListener(this.listenerUpdateApp)
  }
  makeRemoteRequest = async () => {
    const { navigation } = this.props;
    const item = navigation.getParam('item', null);
    var url = appConfig.config.app.SITE_URL + item.additional.url
    //url = 'https://fake384925803459.com/fak'
    //this.props.loadNewsItem(item.uid, url)
    NewsItemModel.load(this.props, item.uid, url).then((newsItem)=>{
      this.isComponentMount && this.setState({
        ...this.state,
        isOnline: true,
        showSave: true,
      })
      try {
        firebaseAnalytics.logEvent('view_item', {
          'uid': newsItem.newsItem.uid,
          'url': newsItem.newsItem.additional.url,
          'title': newsItem.newsItem.title,
        })
          
      } catch (error) {
      }

    }).catch((error)=>{

    })
  }
  async toogleSavedNewsItem(){
    if(!avoidDoubleAction.isActionActive()){
      return
    }
    const { navigation } = this.props;
    const item = navigation.getParam('item', null);
    const uid = item.uid
    const saveNews = (!!this.props.saveNews)
    const newsItem = saveNews? this.state.newsItem : this.props.newsItem.news[uid];
    const isSavedNewsItem = typeof this.props.savedNews.news[uid] !== 'undefined'
    if(isSavedNewsItem){
      this.props.savedNews_remove(uid)
      await newsHelper.removeNews(uid)
    }else{
      var savedItem = newsHelper.calculateSaveItem(newsItem)
      this.props.savedNews_save(uid, savedItem)
      await newsHelper.saveNews(newsItem)
    }
  }
  openAuthor(){
    Animated.timing(                  // Animate over time
      this.state.anim,            // The animated value to drive
      {
        toValue: 1,                   // Animate to opacity: 1 (opaque)
        duration: 500,              // Make it take a while
      }
    ).start(); 
  }
  closeAuthor(){
    Animated.timing(                  // Animate over time
      this.state.anim,            // The animated value to drive
      {
        toValue: 0,                   // Animate to opacity: 1 (opaque)
        duration: 500,              // Make it take a while
      }
    ).start(); 
  }
  setAuthorContentHeight(event){
    this.isComponentMount && this.setState({
      authorContentHeight: event.nativeEvent.layout.height
    })
  }
  setContentWidth(event){
    this.isComponentMount && this.setState({
      contentWidth: event.nativeEvent.layout.width
    })
  }
  renderAuthor(newsItem){
    const author = newsItem.additional.author
    if(!author){
      return null;
    }
    const img = author.image? appConfig.config.app.SITE_URL + author.image.src : null
    const authorHeader1HeightAnim = this.state.anim.interpolate({
      inputRange: [0, 1],
      outputRange: [calculateResponsiveValue(100), calculateResponsiveValue(60)],
    });
    const authorHeader2HeightAnim = this.state.anim.interpolate({
      inputRange: [0, 1],
      outputRange: [0, calculateResponsiveValue(60)],
    });
    const authorImageScaleAnim = this.state.anim.interpolate({
      inputRange: [0, 1],
      outputRange: [calculateResponsiveValue(70)/calculateResponsiveValue(100), 1],
    });
    const authorImageLeftAnim = this.state.anim.interpolate({
      inputRange: [0, 1],
      outputRange: [calculateResponsiveValue(10), 0],
    });
    const authorContentHeightAnim = (typeof this.state.authorContentHeight === 'undefined'? 0: 
    this.state.anim.interpolate({
      inputRange: [0, 1],
      outputRange: [0, this.state.authorContentHeight],
    }))

    const authorImageContainerWidthAnim = (typeof this.state.contentWidth === 'undefined'? null: 
    this.state.anim.interpolate({
      inputRange: [0, 1],
      outputRange: [calculateResponsiveValue(70), this.state.contentWidth],
    }))

    const authorImageContainerRight = typeof this.state.contentWidth === 'undefined'? 0:null
    const authorImageContainerWidth = typeof this.state.contentWidth === 'undefined'? null:this.state.contentWidth
    const authorHeaderLeftAnim = (typeof this.state.contentWidth === 'undefined'? calculateResponsiveValue(70): 
    this.state.anim.interpolate({
      inputRange: [0, 1],
      outputRange: [calculateResponsiveValue(70), this.state.contentWidth + calculateResponsiveValue(10)],
    }))
    const authorHeaderWidth = typeof this.state.contentWidth === 'undefined'? null:this.state.contentWidth - calculateResponsiveValue(70)

    return (
      <View style={styles.authorContainer}>
      <View onLayout={this.setContentWidth} style={{}}>
        <Animated.View style={[styles.authorHeader1, {height: calculateResponsiveValue(100),}, {height: authorHeader1HeightAnim}]} />
        <Animated.View style={[styles.authorHeader2, {height: authorHeader2HeightAnim}]} />
        <Animated.View style={[styles.authorImageContainer, {right: authorImageContainerRight, width: authorImageContainerWidthAnim, left: authorImageLeftAnim}]}>
          <Animated.View style={{transform: [{ scale: authorImageScaleAnim }]}}>
            {img && 
              <Thumbnail circular size={55} style={[styles.authorImage, {width: calculateResponsiveValue(100), height: calculateResponsiveValue(100), borderRadius: calculateResponsiveValue(100)/2,}]} source={{uri:img}} />
            }
          </Animated.View>
        </Animated.View>
        <Animated.View style={[styles.authorHeader, {width: authorHeaderWidth, left: authorHeaderLeftAnim}]}>
          <H1 style={styles.headerAuthorName}>{author.name}</H1>
          {author.detail.email && <View><Text style={styles.headerAuthorEmail}>{author.detail.email}</Text></View>}
          <View style={styles.flechaRojaAbajoContainer}>
            <Button bordered style={styles.headerButton} onPress={this.openAuthor}>
              <Image style={styles.flechaRojaAbajo} source={Assets.flechaRojaAbajo} />
            </Button>
          </View>
        </Animated.View>

      </View>

      <Animated.View
            style={[styles.authorContentAnim,{height: authorContentHeightAnim}]}>
        <View onLayout={this.setAuthorContentHeight} style={[styles.authorContent]}>
          <H1 style={[styles.authorName]}>{author.name}</H1>
          {author.detail.email && <View><Text style={[styles.authorText, styles.authorEmail]}>{author.detail.email}</Text></View>}
          <View style={styles.authorLine} />
          <HTML 
            html={author.bio} 
            stylesConfiguration='authorBioConfiguration'
          />
          <View style={styles.flechaBlancaArribaContainer}>
            <Button bordered style={styles.headerButton} onPress={this.closeAuthor}>
              <Image style={styles.flechaBlancaArriba} source={Assets.flechaBlancaArriba} />
            </Button>
          </View>

        </View>
      </Animated.View>
      
      </View>
    )
  }

  renderItem(newsItem){

    if(typeof newsItem === 'undefined'){
      return null
    }
    const item = newsItem

    var NEWS_TYPE = appConfig.config.app.detail.NEWS_TYPE
    var CONTENT_TYPE = appConfig.config.app.detail.CONTENT_TYPE

    const title = '<h1>'+item.title+'</h1>'
    const teaser = '<div class="teaser-wraper"><p>'+item.additional.teaser+'</p></div>'
    const date = moment(item.datetime).format(appConfig.config.app.SONDEOS.formatDate).toUpperCase()
    const seat = (!!item.additional.seat)? ' | '+item.additional.seat.toUpperCase() : ''
    const dateSeat = '<div class="wraper-date"><p class="date">'+date + seat+'</p></div>'
    const mainCategory = (!!item.additional.mainCategory)? '<div class="wraper-category"><p class="category">'+item.additional.mainCategory.toUpperCase()+'</p></div>' : null
    return (
      <View>
        {this.renderAuthor(newsItem)}
        <Multimedia {...this.props} item={newsItem} NEWS_TYPE={NEWS_TYPE} CONTENT_TYPE={CONTENT_TYPE} />
        
        {mainCategory && 
          <HTML stylesConfiguration='detailConfiguration' html={mainCategory} />
        }
          
          <View style={[styles.wrapperAuthor]}>
          
          <HTML stylesConfiguration='detailConfiguration' html={title} />
        <HTML stylesConfiguration='detailConfiguration' html={dateSeat} />
      
        <HTML stylesConfiguration='detailTeaserConfiguration'
                          html={teaser}/>
          <View style={styles.decoration} />                 
                        
        <HTML 
          stylesConfiguration='detailConfiguration'
          html={newsItem.additional.contentElements} 
        />
        </View>
        
      </View>
    )
  }
  onShare() {
    if(!avoidDoubleAction.isActionActive()){
      return
    }
    const { navigation } = this.props;
    const item = navigation.getParam('item', null);
    const newsItem = this.props.newsItem.news[item.uid];
    const url = newsItem.additional.originalUrl
    let shareOptions = {
      title: "UN Periódico: "+newsItem.title,
      message: url,
      //url: this.props.item.url,
      subject: "UN Periódico: "+newsItem.title, //  for email
    };

    setTimeout(() => {
      Share.open(shareOptions).catch((err) => {});
    },300);
  }
  renderError(){
    return(
      <CustomError 
        error={this.props.newsItem.error} 
        buttonText="Recargar" 
        onPress={()=>{this.makeRemoteRequest()}}
        icon = {{name: 'ios-refresh'}} 
        />
    )
  }
  isLoading(){
    const saveNews = (!!this.props.saveNews)
    if(saveNews){
      return (!!this.state.loading)
    }
    const loading = typeof this.props.newsItem.loading !== 'undefined'? this.props.newsItem.loading:(!!this.state.loading)
    return loading
  }
  render() {
    const { navigation } = this.props;
    const item = navigation.getParam('item', null);
    const uid = item.uid
    const saveNews = (!!this.props.saveNews)
    const newsItem = saveNews? this.state.newsItem : this.props.newsItem.news[uid];
    const loading = this.isLoading()
    const isSavedNewsItem = typeof this.props.savedNews.news[uid] !== 'undefined'
    const isError = this.props.newsItem.error !== null && typeof newsItem === 'undefined'

    const showOffline = saveNews? false : (!this.state.isOnline && typeof newsItem !== 'undefined' && !loading)
    const showSaveButton = !loading && !isError && (!!newsItem)
    

    return (
        <StyleProvider style={getTheme(unperiodicoVariables)}>
        <Container>



          <CustomHeader>
            <Left>
              <Bar navigation={this.props.navigation} />
            </Left>
            <Body>
            </Body>
            <Right>
              {showSaveButton && 
              <Button bordered style={styles.headerButton} onPress={()=>{this.toogleSavedNewsItem()}}>
              {!isSavedNewsItem && this.state.showSave && 
              <Icon type="FontAwesome" name="bookmark-o" style={styles.headerIcon} />
              }
              {isSavedNewsItem && 
              <Icon type="FontAwesome" name="bookmark" style={styles.headerIcon} />
              }
              
            </Button>
            }

          </Right>
          </CustomHeader>
          <Content 
            refreshControl={
              <CustomRefreshControl
                refreshing={loading}
                onRefresh={()=>{
                  this.makeRemoteRequest()
                }}
              />
            }
        >
            {showOffline && 
              <Message message='Sin conexión' />
            }
  
            {isError && this.renderError()}
            {!loading && !isError && this.renderItem(newsItem)}
  
         
  
            
          </Content>
          {!loading && !isError && 
          <Fab
          active={true}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: unperiodicoVariables.titleFontColor }}
          position="bottomRight"
          onPress={() => this.onShare()}
        >
          <IconNB name="md-share" />
        </Fab>
        }
        </Container>
        </StyleProvider>
      );
  }

  
}

export default Author

import React, { Component } from "react";
import { View, StatusBar, WebView } from "react-native";
import { Container, Content, Header, Body, Left, Right, Text,
  StyleProvider,Button, Title
 } from "native-base";
 

import Bar from '../bar/bar'

import styles from "./styles";
import getTheme from '../../../native-base-theme/components'
import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import appConfig from '../../models/appConfig'
import avoidDoubleAction from "../../services/avoidDoubleAction";
import CustomStatusBar from "../../components/CustomStatusBar";
import { calculateResponsiveValue } from "../../helpers/Responsive";
import CustomHeader from "../../components/CustomHeader";



class Poll extends Component<{}, State> {
  constructor(props) {
    super(props);
    
  }
  

  render() {
    return (
        <StyleProvider style={getTheme(unperiodicoVariables)}>
        <Container  style={{flex: 1, backgroundColor: 'red'}}>



          <CustomHeader>
          <CustomStatusBar />
            <Left>
              <Bar navigation={this.props.navigation} />
            </Left>
            <Body>
            </Body>
            <Right>
            </Right>
          </CustomHeader>
          <Content contentContainerStyle={{ flex: 1 }} style={{}}>
  
          <View style={styles.container}>
          <View style={styles.content}>
            <WebView
                source={{uri: appConfig.config.app.POLL_URL}}
              />
          </View>
            <View style={styles.buttonsContainer}>
              <Button primary onPress={()=>{
                      if(!avoidDoubleAction.isActionActive()){
                        return
                      }
                      this.props.navigation.push ('NewsContainer',{
                        url: appConfig.config.app.POLL_LIST_URL,
                        view: 'Sondeos',
                        category: 'staticsondeos',
                        key: 'staticsondeos',
                      });

              }}><Text> VER RESULTADOS </Text></Button>
            </View>

          </View>

            
          </Content>
  
        </Container>
        </StyleProvider>
      );
  }

  
}

export default Poll

import React, { Component } from "react";
import { Image } from "react-native";
import { Icon, Button } from "native-base";

import styles from "./styles";
import Assets from '../../../assets'
import avoidDoubleAction from "../../services/avoidDoubleAction";
import { calculateResponsiveValue } from "../../helpers/Responsive";

class Bar extends Component<{}, State> {

  render() {
    const { state } = this;
    const showLogo = typeof this.props.showLogo === 'undefined' || this.props.showLogo

    return (
      <Button
      bordered style={styles.headerButton}
      onPress={() => {
        if(!avoidDoubleAction.isActionActive()){
          return
        }

        this.props.navigation.openDrawer()
      }}
    >
      <Icon style={styles.headerIcon} name="ios-menu" />
      {showLogo &&
        <Image
        style={{height: calculateResponsiveValue(30, 45), width: calculateResponsiveValue(106, 159), }}
        resizeMode={'contain'}
        source={Assets.logo}>
        </Image>
      }

    </Button>
    );
  }
}

export default Bar;

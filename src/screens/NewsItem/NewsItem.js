import React, { Component } from 'react'
import {
  NetInfo,
  View,
  ListView,
  StyleSheet,
  ScrollView,
  FlatList,
  ActivityIndicator,
  RefreshControl
} from 'react-native'
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  List,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  H1,
  H2,
  H3,
  Right,
  Spinner
} from "native-base";

import styles from "./styles"
import config from '../../../config'
import HTML from '../../components/HTML'
import styleVariables from '../../../native-base-theme/variables/unperiodico-material'
import CustomStatusBar from '../../components/CustomStatusBar';
import CustomHeader from '../../components/CustomHeader';


export default class NewsItem extends Component {
  constructor(props) {
    super(props)

  }

  componentWillMount() {
    return
    this.dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    this.props.loadOfflineItems()
    this.makeRemoteRequest(true)
    this.dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})

    if (NetInfo) {
      NetInfo.isConnected.fetch().done(isConnected => {
        if (isConnected) {
          this.props.checkConnection()
        } else {
          this.props.goOffline()
        }
      })
    } else {
      this.props.checkConnection()
    }
  }
  makeRemoteRequest = (initial, refresh) => {
    let nextPage
    var isPid = typeof this.props.pid !== 'undefined'
    var isCategory = typeof this.props.category.item !== 'undefined'

    if(!initial){
      if(isCategory){
        nextPage = this.props.news[this.getCategory()].nextPage
      }else if(isPid){
        nextPage = this.props.news[this.props.category].nextPage
      }
      
    }
    if(!initial && !nextPage){
      return
    }



    let params = {
      type: 102,
      tx_news_pi1: {
      }  
    };
    if(isCategory){
      params.tx_news_pi1['overwriteDemand'] = {
        categories: this.props.category.item.uid
      }
    }
    if(!initial){
      params.tx_news_pi1['@widget_0'] = {
        currentPage: nextPage
      }
    }
    if(isCategory){
      this.props.loadNewsFromGenerateUrl(this.getCategory(), 95, params, refresh)
    }else if(isPid){
      this.props.loadNewsFromGenerateUrl(this.getCategory(), this.props.pid, params, refresh)
    }
        

  }

  
  render() {
  

    return(
      <Container style={styles.container}>
        <CustomHeader>
        <CustomStatusBar />
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Basic Card</Title>
          </Body>
          <Right />
        </CustomHeader>

        <Content >
          <Card style={styles.mb}>
            <CardItem>
              <Body>
                <Text>This is just a basic card with some text to boot.</Text>
                <Text>Like it? Keep Scrolling...</Text>
              </Body>
            </CardItem>
          </Card>
        </Content>
      </Container>

      
    )
     
    
    
    
  }
}


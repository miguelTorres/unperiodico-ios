import { calculateResponsiveValue } from "../../helpers/Responsive";

export default {
  container: {
    backgroundColor: "#FFF"
  },
  headerButton: {
    borderWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },
  headerIcon: {
    marginLeft: 1,
    marginRight: calculateResponsiveValue(10, 30),
    fontSize: calculateResponsiveValue(25, 30),  
  },

};

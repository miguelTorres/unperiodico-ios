import React, { Component } from "react";
import { StatusBar, RefreshControl, Image, View, Alert } from "react-native";
import { Container, Content, Header, Body, Left, Right, StyleProvider, Toast, Button, Icon, Text } from "native-base";
import Bar from './bar'
import getTheme from '../../../native-base-theme/components'
import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import Assets from '../../../assets'

import FCM from '../../models/fcm'


import styles from "./styles";

import AppConfigModel from '../../models/appConfig'
import LoadOfflineModel from '../../models/loadOffline'
import CustomError from '../../components/error/CustomError'

import CategoriesModel from '../../models/categories'
import appService from "../../services/appService";
import PermissionModel from "../../models/permissionModel";
import { calculateResponsiveValue } from "../../helpers/Responsive";
import CustomStatusBar from "../../components/CustomStatusBar";
import CustomHeader from "../../components/CustomHeader";
import CustomRefreshControl from "../../components/CustomRefreshControl";


class Loading extends Component<{}, State> {
  constructor(props) {
    super(props);
    const { navigation } = this.props;
    const error = navigation.getParam('error', null);
    this.state = {
      isLoading: !error,
      error
    }
  }

  componentDidMount = async () => {
    this.isComponentMount = true
    const { navigation } = this.props;
    const error = navigation.getParam('error', null);
    if(error===null){
      await this._bootstrap();
    }
    
  }
  componentWillMount = async () => {
    this.isComponentMount = false
  }

  loadAppConfigError(){
    this.isComponentMount && this.setState({
      isLoading: false,
      error: 'No se cargó la información. Por favor revise su conexión'
    })
  }

  loadOffline (){
    AppConfigModel.loadOffline(this.props).then(()=>{
      if(this.props.configuration.configuration !== null && Object.keys(this.props.configuration.configuration).length>0){
        this.props.navigation.navigate ('App');
      }else{
        this.loadAppConfigError()
      }
      
    }).catch(()=>{
      this.loadAppConfigError()
    })
  }

  loadMultipleOffline(){
    this.props.loadSavedNews()
    this.props.loadOfflineNews()
    this.props.loadOfflineNewsItem()
  }

  loadAppConfiguration = async (goToApp = true) => {
    try {


      AppConfigModel.load(this.props).then(()=>{
        /*return Promise.all([
          CategoriesModel.load(this.props),

        ])*/
        return CategoriesModel.load(this.props)
      }).then((values)=>{
        if(Object.keys(this.props.categories.categories).length >0 &&
        typeof this.props.configuration.configuration.app !== 'undefined'){
          if(goToApp){
            this.props.navigation.navigate ('App');
          }
        }else{
          if(goToApp){
            this.isComponentMount && this.setState({
              isLoading: false,
              error: 'No se ha cargado información. Por favor revise su conexión 2'
            })
          }
        }
  

      }).catch((error)=>{
        if(goToApp){
          this.isComponentMount && this.setState({
            isLoading: false,
            error: 'No se ha cargado información. Por favor revise su conexión'
          })
  
        }
      })
      //this.loadCategoriesModel()
      
    } catch (error) {
      if(goToApp){
        this.isComponentMount && this.setState({
          isLoading: false,
          error: 'No se ha cargado información. Por favor revise su conexión 4'
        })
      }

      //this.loadOffline()
    }    
  }

  _bootstrap = async () => {

    if(typeof this.props.configuration.configuration.app !== 'undefined'){
      this.props.navigation.navigate ('App');
      return
    }


    var writeStoragePermission = await PermissionModel.requestWriteExternalStoragePermission()
    var readStoragePermission = await PermissionModel.requestReadExternalStoragePermission()


    this.isComponentMount && this.setState({
      isLoading: true,
      error: null
    })

    if(writeStoragePermission && readStoragePermission){
      Promise.all([
        AppConfigModel.loadOffline(this.props),
        LoadOfflineModel.loadOfflineCategories(this.props)
      ]).then((values)=>{
        var configuration = values[0]
        var categories = values[1]
        this.loadMultipleOffline()
        if(Object.keys(this.props.categories.categories).length >0 &&
        typeof this.props.configuration.configuration.app !== 'undefined'){
          this.loadAppConfiguration(false)

          this.props.navigation.navigate ('App');
        }else{
          this.loadAppConfiguration()
        }
      }).catch((error)=>{
        this.loadAppConfiguration()
      })
      
      //this.props.navigation.navigate ('App');
    }else{
      this.loadAppConfiguration()
    }
  };
  
  render() {
    return (
      <StyleProvider style={getTheme(unperiodicoVariables)}>
      <Container>
      <CustomHeader style={{height: calculateResponsiveValue(54, 65)}}>
      <CustomStatusBar />
      <Left>
      <Button
                bordered style={styles.headerButton}
                onPress={() => {
                  
                }}
              >
      <Image
              style={{height: calculateResponsiveValue(30, 45), width: calculateResponsiveValue(106, 159), }}
              source={Assets.logo}>
              </Image>
              </Button>
        </Left>
        <Body>
        </Body>
        <Right>
          </Right>
      </CustomHeader>
      <Content  refreshControl={
        <CustomRefreshControl
          refreshing={this.state.isLoading}
        />
      }>
        {this.state.error !== null ? (
                  <CustomError 
                  error={this.state.error} 
                  buttonText="Recargar" 
                  onPress={()=>{this._bootstrap()}}
                  icon = {{name: 'ios-refresh'}}
                  />
        ) : (
              null
        )}          
       
        </Content>
  
    </Container>  
    </StyleProvider>
    );    
  }
}

export default Loading

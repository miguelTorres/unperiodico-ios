class Permission {
  async requestReadExternalStoragePermission() {
    return true
  }
  async requestWriteExternalStoragePermission() {
    return true
  }
  async requestPermission(data) {
    return true
  }  

}
let PermissionModel = new Permission();
export default PermissionModel;

import { Platform } from "react-native";
import firebase from 'react-native-firebase';
import config from '../../config'
import Ping from "../api/ping";
import appConfig from "./appConfig";


class FirebaseAnalytics {
    logEvent(event, params){
        //firebase.analytics().setAnalyticsCollectionEnabled(true)
        firebase.analytics().logEvent(event, params)
    }
}
let firebaseAnalytics = new FirebaseAnalytics();
export default firebaseAnalytics;
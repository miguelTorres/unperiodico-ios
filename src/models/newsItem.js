import jsonApi from '../api/jsonApi'

class NewsItemModel {


  load = async (props, uid, url) => {
    props.loadingNewsItem(uid)
    try {
      let news = await jsonApi.getAll(url)
      props.loadNewsItemSuccess(uid, news)
      return news
    } catch (error) {
      props.loadNewsItemError(uid, 'La noticia no fue cargada. Por favor revise su conexión.')
      throw error;
    }
  };

}
let newsItemModel = new NewsItemModel();
export default newsItemModel;

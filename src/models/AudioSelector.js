import FloatingViewConstants from '../components/FloatingViewConstants'
import DefaultSelector from './DefaultSelector'
export default class AudioSelector extends DefaultSelector {
  constructor(item) {
    super(item)
    this.type = 'Audio'
  }
  isSelected(){
    return this.isCE() && typeof this.item.additional.contentElementShowInListIdList.audios !== 'undefined'
  }
  getMedias(){
    return this.item.additional.contentElementShowInListIdList.audios
  }
  getInitialFloatingState(){
    return FloatingViewConstants.layout.MINIMUM
  }
  getIcon(){
    return {
      name: 'volume-2',
      type: 'Feather'
    }
  }

}
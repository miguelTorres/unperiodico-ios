import offline from 'react-native-simple-store'
import config from '../../config'
import jsonApi from '../api/jsonApi'
import initialState from '../reducers/initialState'

class AppConfig {
  config = null
  loadOffline = (props) => {
    return new Promise((resolve, reject) => {
      offline.get('configuration').then(configuration => {
        this.set(configuration)
        props.loadOfflineConfigurationSuccess(configuration || initialState.configuration.configuration)
        resolve(props.configuration)
      }).catch(error=>{
        props.loadOfflineConfigurationError('Error cargando las noticias guardadas')
        reject(error)
      })
  
    });


  }
  load = async (props) => {
    const url = config.CONFIG_URL
    const reqSetting = {
      headers: {
        Accept: 'application/json',
      },
    };
    props.loadingConfiguration()
    try {
      let response = await jsonApi.getAll(url)
      if(typeof response!=='undefined' && Object.keys(response).length>0){
        this.config = response
        props.loadConfigurationSuccess(response)
      }else{
        props.loadConfigurationError('La configuración no se pudo cargar')
      }
      
      return response;
    } catch (error) {
      props.loadConfigurationError('La configuración no se pudo cargar')
      throw error;
    }
  };
  get = () => this.config  
  set = (config) => {this.config = config}
}
let appConfig = new AppConfig();
export default appConfig;

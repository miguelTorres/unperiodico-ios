import DefaultSelector from './DefaultSelector'
import appConfig from './appConfig'
import FloatingViewConstants from '../components/FloatingViewConstants';
export default class VideoSelector extends DefaultSelector {
  constructor(item) {
    super(item)
    this.type = 'Video'
  }
  isSelected(){
    return this.isCE() && typeof this.item.additional.contentElementShowInListIdList.videos !== 'undefined'
  }
  getMedias(){
    return this.item.additional.contentElementShowInListIdList.videos
  }
  /*
  getImageUrlByMedia(media){
    return appConfig.config.app.THUMBNAILS_URL + media.sources[0].src + '.png'
  }*/
  getIcon(){
    return {
      name: 'play',
      type: 'Feather'
    }
  }
  getInitialFloatingState(){
    return FloatingViewConstants.layout.MINIMIZED
  }
  getBlock(){
    return false
  }


}
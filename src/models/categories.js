import jsonApi from '../api/jsonApi'
import appConfig from './appConfig'


class Categories {

  resource = async () => {
    try {
      const url = appConfig.config.app.CATEGORIES_URL
      //const url = 'https://fakejfaksf.com/falsldlf'
      const categories = await jsonApi.getAll(url)
      return categories;
    } catch (error) {
      throw error;
    }
  };
  load(props){
    return new Promise((resolve, reject) => {
      this.resource().then((categories)=>{
        props.loadCategoriesSuccess(categories)
        resolve(categories)
      }).catch((error)=>{
        props.loadCategoriesError(error)
        reject(error)
      })
    });
  }

}
let categories = new Categories();
export default categories;
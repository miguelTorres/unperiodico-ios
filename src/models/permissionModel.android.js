import { PermissionsAndroid } from 'react-native';
class Permission {
  async requestReadExternalStoragePermission() {
    return await this.requestPermission({
      permission: PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      title: 'UN Periódico solicitud de permiso',
      message: 'UNPeriodico necesita permiso ' +
                 'para leer las noticias almacenadas.'
    })
  }
  async requestWriteExternalStoragePermission() {
    return await this.requestPermission({
      permission: PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      title: 'UN Periódico solicitud de permiso',
      message: 'UNPeriodico necesita permiso ' +
                 'para almacenar las noticias.'
    })
  }
  async requestPermission(data) {
    var {permission, title, message} = data
    try {
      const granted = await PermissionsAndroid.request(
        permission,
        {
          'title': title,
          'message': message
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        return true
      } else {
        return false
      }
    } catch (err) {
      return false
    }
  }  

}
let PermissionModel = new Permission();
export default PermissionModel;

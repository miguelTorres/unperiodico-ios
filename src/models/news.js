import jsonApi from '../api/jsonApi'
import PaginateNews from '../helpers/PaginateNews'
class NewsModel {


  load = async (props, category, url, reset, jsonkey) => {
    props.loadingNews(category);
    try {
      let news = await jsonApi.getAll(url)
      if(typeof(jsonkey) !== 'undefined' && jsonkey){
        props.loadNewsSuccess(category, news[jsonkey], reset);
      }else{
        props.loadNewsSuccess(category, news, reset);
      }
  } catch (error) {
      props.loadNewsError(category, 'No se cargaron las noticias. Por favor revise su conexión.');
      throw error;
    }
  };
  loadFiltered = async (newsContainer, category, url, reset, jsonkey) => {
    try {
      let news = await jsonApi.getAll(url)
      let filteredNews
      if(typeof(jsonkey) !== 'undefined' && jsonkey){
        filteredNews = news[jsonkey]
      }else{
        filteredNews = news
      }
      newObject = Object.assign({}, newsContainer)
      PaginateNews.loadNews(newObject, category, filteredNews, reset)
      return newObject
  } catch (error) {
      throw error;
    }
  };
}
let newsModel = new NewsModel();
export default newsModel;

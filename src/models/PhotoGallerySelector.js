import FloatingViewConstants from '../components/FloatingViewConstants'
import DefaultSelector from './DefaultSelector'
import _ from "lodash";
import appConfig from './appConfig'

export default class PhotoGallerySelector extends DefaultSelector {
  constructor(item) {
    super(item)
    this.type = 'PhotoGallery'
  }
  isSelected(){
    return this.isCE() && typeof this.item.additional.contentElementShowInListIdList.images !== 'undefined'
  }
  getMedias(){
    return this.item.additional.contentElementShowInListIdList.images
  }
  getUrl(media){
    return appConfig.config.app.SITE_URL + media.src
  }

  getImages(){
    const medias = this.getMedias()

    var images = []

    /*return _(medias).toArray().map( (media)=>{
      var uri = this.getUrl(media)
      return { source: { uri } }
    }).values()*/
    
    return _(medias).mapValues((media)=>{
      var uri = this.getUrl(media)
      var figCaption = (!!media.figCaption)?media.figCaption:null
      return { source: { uri }, figCaption }
    })
    .values()
    .value()
  }
    
  
  getInitialFloatingState(){
    return FloatingViewConstants.layout.MAXIMIZED
  }
  getIcon(){
    return {
      name: 'md-photos',
      type: 'Ionicons'
    }
  }

}
import { Platform } from "react-native";
import firebase from 'react-native-firebase';
import config from '../../config'
import Ping from "../api/ping";
import appConfig from "./appConfig";
import { EventRegister } from "react-native-event-listeners";
import firebaseConstants from "./firebaseConstants";


class FirebaseDatabase {
    constructor(){
        this.isInit = false;
    }
    init(){
        if(this.isInit){
            return
        }
        this.isInit = true;
        firebase.database().ref('app').on('value', (value)=>{
            var val = value.val()
            EventRegister.emit(firebaseConstants.MANAGE_UPDATE_APP, val)

          })
    }
}
let firebaseDatabase = new FirebaseDatabase();
export default firebaseDatabase;
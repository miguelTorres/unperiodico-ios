import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as RootActions from '../actions/root'
import * as ItemsActions from '../actions/items'
import * as NewsActions from '../actions/news'
import * as NewsItemActions from '../actions/newsItem'
import * as SavedNewsActions from '../actions/savedNews'
import * as AppConfigActions from '../actions/appConfig'
import * as CategoriesActions from '../actions/categories'
import * as ConnectionActions from '../actions/connection'
import * as LayoutActions from '../actions/layout'





class ConnectComponentService {
    constructor(){
    }
    createVariableHeaderComponent(component, category){
        
        function mapStateToProps(state) {
            return {
                newsByCategory: state.news.news[category],
                connected: true
            }
        }
        
        function mapDispatchToProps(dispatch) {
            return bindActionCreators(Object.assign({}, NewsActions), dispatch)
        }  
        return connect(mapStateToProps, mapDispatchToProps)(component)
      }

    createNewsComponent(component, category){
        function mapStateToProps(state) {
          return {
            newsByCategory: state.news.news[category],
            onlineLoaded: state.news.onlineLoaded[category],
            //onlineItems: state.items.onlineList,
            //offlineItems: state.items.offlineList,
            //news: state.news,
            //newsByCategory: Object.assign({}, state.news[category]),
            categories: state.categories,
            savedNews: state.savedNews,
            configuration: state.configuration,
            connectionChecked: state.connection.connectionChecked,
            connected: state.connection.connected,
            layout: state.layout
          }
        }
        
        function mapDispatchToProps(dispatch) {
            return bindActionCreators(Object.assign({}, RootActions, ItemsActions, NewsActions, SavedNewsActions, AppConfigActions, CategoriesActions, LayoutActions), dispatch)
        }  
        return connect(mapStateToProps, mapDispatchToProps)(component)
      }
      createGeneralComponent(component){
        function mapStateToProps(state) {
            return {
              //news: state.news,
              newsItem: state.newsItem,
              categories: state.categories,
              savedNews: state.savedNews,
              configuration: state.configuration,
              connectionChecked: state.connection.connectionChecked,
              connected: state.connection.connected,
              layout: state.layout
            }
          }
          
          function mapDispatchToProps(dispatch) {
              return bindActionCreators(Object.assign({}, RootActions, ItemsActions, NewsActions, NewsItemActions, SavedNewsActions, AppConfigActions, CategoriesActions, ConnectionActions, LayoutActions), dispatch)
          }  
          
          return connect(mapStateToProps, mapDispatchToProps)(component)
      }

}
let connectComponentService = new ConnectComponentService();
export default connectComponentService;

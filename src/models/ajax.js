import ajaxApi from '../api/ajaxApi'

class Ajax {

  load = async (url) => {
    try {
      const response = await ajaxApi.getAll(url).then((response)=>{
        //Alert.alert('response', response)
        return response
      }).catch((error)=>{
        throw error
      })
      return response;
    } catch (error) {
      throw error;
    }
  };    


}
let ajax = new Ajax();
export default ajax;

import offline from 'react-native-simple-store'
import config from '../../config'
import jsonApi from '../api/jsonApi'
import _ from 'lodash'


class LoadOffline {
  loadOfflineConfiguration = (props) => {

    return new Promise((resolve, reject) => {
      offline.get('configuration').then(configuration => {
        AppConfigModel.set(configuration)
        props.loadOfflineConfigurationSuccess(configuration || {})
        resolve(props.configuration.configuration)
      }).catch(error=>{
        props.loadOfflineConfigurationError('Error cargando las noticias guardadas')
        reject(error)
      })
  
    });


  }
  loadOfflineCategories(props) {


    return new Promise((resolve, reject) => {
      
      offline.get('categories').then(categories => {
        props.loadOfflineCategoriesSuccess(categories || {})
        resolve(categories)
      }).catch(error=>{
        props.loadOfflineCategoriesError('Error cargando las categorías offline')
        reject(error)
      })
  
    });


  }
    
}
let loadOffline = new LoadOffline();
export default loadOffline;

import {
  LOAD_NEWS_SUCCESS,
  LOADING_NEWS,
  LOAD_NEWS_ERROR,
  RESET_NEWS,
  NEWS_OFFLINE_LOAD_SUCCESS,
  NEWS_OFFLINE_LOAD_ERROR
} from '../actions/news'


import initialState from './initialState'
import PaginateNews from '../helpers/PaginateNews'


export default function reducer(state = initialState.news, action) {
  let list, newObject


  switch (action.type) {

    case NEWS_OFFLINE_LOAD_ERROR:
      return {
        ...state,
        error: action.error
      }

    case NEWS_OFFLINE_LOAD_SUCCESS:
      return {
        ...state,
        offlineLoaded: true,
        error: null,
        news: action.news
      }

    case LOADING_NEWS:
      newObject = Object.assign({}, state.news)
      PaginateNews.loadingNews(newObject, action.category)
      return {...state, news: newObject}
    case RESET_NEWS:
      newObject = Object.assign({}, state.news)
      PaginateNews.resetNews(newObject, action.category)
      return {...state, news: newObject}
  case LOAD_NEWS_ERROR:
      newObject = Object.assign({}, state.news)
      PaginateNews.loadNewsError(newObject, action.category, action.error)
      return {...state, news: newObject}
    
  case LOAD_NEWS_SUCCESS:
    newObject = Object.assign({}, state.news)
    PaginateNews.loadNews(newObject, action.category, action.news, action.refresh)
    list = {...state.onlineLoaded}
    list[action.category] = true
    
    return {
      ...state, 
      news: newObject,
      onlineLoaded: list
    }
  default:
    return state
  }
}

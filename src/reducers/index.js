import { combineReducers } from 'redux'
import items from './items'
import news from './news'
import newsItem from './newsItem'
import savedNews from './savedNews'
import categories from './categories'
import connection from './connection'
import layout from './layout'
import configuration from './appConfig'

const allReducers = combineReducers({
  items,
  news,
  newsItem,
  savedNews,
  categories,
  connection,
  layout,
  configuration
})

const rootReducer = (state, action) => {
  if (action.type === 'RESET_APP') {
    state = undefined
  }

  return allReducers(state, action);
};
export default rootReducer
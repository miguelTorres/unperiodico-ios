import {
  LOAD_NEWS_ITEM_SUCCESS,
  LOADING_NEWS_ITEM,
  LOAD_NEWS_ITEM_ERROR, 
  NEWS_ITEM_OFFLINE_LOAD_SUCCESS,
  NEWS_ITEM_OFFLINE_LOAD_ERROR
} from '../actions/newsItem'


import initialState from './initialState'
import PaginateNews from '../helpers/PaginateNews'


export default function reducer(state = initialState.newsItems, action) {
  let list, newObject, uid, newsItem, online

  switch (action.type) {

    case NEWS_ITEM_OFFLINE_LOAD_ERROR:
      return {
        ...state,
        error: action.error
      }

    case NEWS_ITEM_OFFLINE_LOAD_SUCCESS:
      return {
        ...state,
        offlineLoaded: true,
        error: null,
        news: action.news
      }

    case LOADING_NEWS_ITEM:
    return {
        ...state,
        loading: true,
        error: null
      }
    case LOAD_NEWS_ITEM_ERROR:
    return {
        ...state,
        loading: false,
        error: action.error
      }

    case LOAD_NEWS_ITEM_SUCCESS:
      list = {...state.news}
      list[action.uid] = action.newsItem.newsItem
      online = {...state.online}
      online[action.uid] = true
      return {
        ...state,
        loading: false,
        online: online,
        news: list,
        error: null
      }

    default:
      return state
    }
}

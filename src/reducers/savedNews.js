import {
  LOAD_SAVED_NEWS_SUCCESS,
  LOAD_SAVED_NEWS_ERROR,
  SAVED_NEWS_SAVE,
  SAVED_NEWS_REMOVE
} from '../actions/savedNews'
import moment from 'moment'
require('moment/locale/es');

import initialState from './initialState'



export default function reducer(state = initialState.savedNews, action) {
  let list, newObject, uid, newsItem

  switch (action.type) {
    case LOAD_SAVED_NEWS_ERROR:
      return {
        ...state,
        error: action.error
      }

    case LOAD_SAVED_NEWS_SUCCESS:
      return {
        ...state,
        offlineLoaded: true,
        news: action.news
      }
    case SAVED_NEWS_SAVE:
      list = {...state.news}
      list[action.uid]={...action.newsItem, savedTime: moment().unix()}
      return {
        ...state,
        news: list
      }
    case SAVED_NEWS_REMOVE:
      list = {...state.news}
      if(typeof action.uid.length !== 'undefined'){
        for(var i=0; i<action.uid.length; i++){
          delete list[action.uid[i]]
        }
      }else{
        delete list[action.uid]
      }
      
      return {
        ...state,
        news: list
      }

    default:
      return state
    }
}

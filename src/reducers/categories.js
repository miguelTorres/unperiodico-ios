import {
  LOAD_CATEGORIES_SUCCESS,
  LOAD_CATEGORIES_ERROR,
  LOAD_OFFLINE_CATEGORIES_SUCCESS,
  LOAD_OFFLINE_CATEGORIES_ERROR,
} from '../actions/categories'
import initialState from './initialState';



export default function reducer(state = initialState.categories, action) {
  let list
  switch (action.type) {
  case LOAD_CATEGORIES_SUCCESS:
    list = Object.keys(action.categories).map((key, index) => {
      return action.categories[key];
    })
    list.sort(function(a,b){
      return a.sorting - b.sorting;
    });

    return {
      ...state,
      error: false,
      categories: list,
    }
    case LOAD_CATEGORIES_ERROR:
    return {
      ...state,
      error: action.error,
    }

    case LOAD_OFFLINE_CATEGORIES_ERROR:
      return {
        ...state,
        error: action.error
      }

    case LOAD_OFFLINE_CATEGORIES_SUCCESS:
  
      return {
        ...state,
        offlineLoaded: true,
        categories: action.categories
      }


  default:
    return state
  }
}



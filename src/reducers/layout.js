import {
  LAYOUT_TOGGLE_FULLSCREEN,
} from '../actions/layout'

import initialState from './initialState';

export default function reducer(state = initialState.layout, action) {

  switch (action.type) {

  case LAYOUT_TOGGLE_FULLSCREEN:
    
    return {
      ...state,
      isFullScreen: typeof action.isFullScreen !== 'undefined'? action.isFullScreen : !state.isFullScreen
    }

  default:
    return state
  }
}

import offline from 'react-native-simple-store'
import jsonApi from '../api/jsonApi';
import AppConfigModel from '../models/appConfig'
import initialState from '../reducers/initialState'

export const CONFIGURATION_LOADING = 'CONFIGURATION_LOADING'
export const CONFIGURATION_LOAD_SUCCESS = 'CONFIGURATION_LOAD_SUCCESS'
export const CONFIGURATION_RESET = 'CONFIGURATION_RESET'
export const CONFIGURATION_LOAD_ERROR = 'CONFIGURATION_LOAD_ERROR'
export const CONFIGURATION_OFFLINE_LOAD_SUCCESS = 'CONFIGURATION_OFFLINE_LOAD_SUCCESS'
export const CONFIGURATION_OFFLINE_LOAD_ERROR = 'CONFIGURATION_OFFLINE_LOAD_ERROR'

export const CONFIGURATION_LOCAL = 'CONFIGURATION_LOCAL'


export function loadOfflineConfiguration() {
  return dispatch => {
    offline.get('configuration').then(configuration => {
    AppConfigModel.set(configuration)
      dispatch(loadOfflineConfigurationSuccess(configuration || initialState.configuration.configuration))
    }).catch(error=>{
    dispatch(loadOfflineConfigurationError('Error cargando las noticias guardadas'))
    })
  }
}

export function loadOfflineConfigurationSuccess(configuration) {
  return {
    type: CONFIGURATION_OFFLINE_LOAD_SUCCESS,
    configuration: configuration
  };
}
export function loadOfflineConfigurationError(error) {
  return {
    type: CONFIGURATION_OFFLINE_LOAD_ERROR,
    error: error
  };
}


export function resetConfiguration() {
  return {
    type: CONFIGURATION_RESET,
  };
}

export function loadingConfiguration(refresh) {
  return {
    type: CONFIGURATION_LOADING,
    refresh: refresh
  };
}

export function loadConfigurationSuccess(configuration) {
  return {
    type: CONFIGURATION_LOAD_SUCCESS,
    configuration: configuration
  };
}
export function loadConfigurationError(error) {
  return {
    type: CONFIGURATION_LOAD_ERROR,
    error: error
  };
}
export function updateLocalConfiguration(configuration) {
  return {
    type: CONFIGURATION_LOCAL,
    configuration: configuration
  };
}
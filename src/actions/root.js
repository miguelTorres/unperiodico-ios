import offline from 'react-native-simple-store'
import _ from "lodash";
export const RESET_APP = 'RESET_APP'


export function resetApplication() {
  var excludes = []
  return dispatch => {
    offline.keys().then((keys)=>{
      _(keys).filter((key)=>!_.includes(excludes, key))
      .forEach((key)=>{
        offline.delete(key)
      })
      dispatch(resetApp())
    })
    
  }
}

export function resetApp() {
  return {
    type: RESET_APP,
  };
}

import offline from 'react-native-simple-store'

export const LOAD_SAVED_NEWS_SUCCESS = 'LOAD_SAVED_NEWS_SUCCESS'
export const LOAD_SAVED_NEWS_ERROR = 'LOAD_SAVED_NEWS_ERROR'

export const SAVED_NEWS_SAVE = 'SAVED_NEWS_SAVE'
export const SAVED_NEWS_REMOVE = 'SAVED_NEWS_REMOVE'

export function savedNews_save(uid, newsItem) {
  return {
    type: SAVED_NEWS_SAVE,
    uid,
    newsItem
  };
}
export function savedNews_remove(uid) {
  return {
    type: SAVED_NEWS_REMOVE,
    uid,
  };
}

export function loadSavedNews() {
  return dispatch => {
    offline.get('savedNews').then(news => {
      dispatch(loadSavedNewsSuccess(news || {}))
    }).catch(error=>{
      dispatch(loadSavedNewsError('Error cargando las noticias guardadas'))
    })
  }
}

export function loadSavedNewsSuccess(news) {
  return {
    type: LOAD_SAVED_NEWS_SUCCESS,
    news
  };
}
export function loadSavedNewsError(error) {
  return {
    type: LOAD_SAVED_NEWS_ERROR,
    error
  };
}

import offline from 'react-native-simple-store'
import jsonApi from '../api/jsonApi';
import _ from "lodash";
export const LOAD_NEWS_SUCCESS = 'LOAD_NEWS_SUCCESS'
export const LOADING_NEWS = 'LOADING_NEWS'
export const RESET_NEWS = 'RESET_NEWS'
export const LOAD_NEWS_ERROR = 'LOAD_NEWS_ERROR'

export const NEWS_OFFLINE_LOAD_SUCCESS = 'NEWS_OFFLINE_LOAD_SUCCESS'
export const NEWS_OFFLINE_LOAD_ERROR = 'NEWS_OFFLINE_LOAD_ERROR'

export function loadOfflineNews() {
  return dispatch => {
    offline.keys().then((keys)=>{
      _(keys).forEach((key)=>{
        offline.get(key).then((info)=>{
        })
        
      })
      
    })

    offline.get('news').then(news => {
      dispatch(loadOfflineNewsSuccess(news || {}))
    }).catch(error=>{
      dispatch(loadOfflineNewsError('Error cargando las noticias offline.'))
    })
  }
}



export function loadOfflineNewsSuccess(news) {
  return {
    type: NEWS_OFFLINE_LOAD_SUCCESS,
    news
  };
}
export function loadOfflineNewsError(error) {
  return {
    type: NEWS_OFFLINE_LOAD_ERROR,
    error
  };
}


export function loadNews(category, url, reset, jsonkey) { 
  //url = 'https://fasdfkasd.com/asdfjkjd'
  return function(dispatch) {
    dispatch(loadingNews(category));
    return jsonApi.getAll(url).then(news => {
      if(typeof(jsonkey) !== 'undefined'){
          dispatch(loadNewsSuccess(category, news[jsonkey], reset));
      }else{
        dispatch(loadNewsSuccess(category, news, reset));
      }
      
    }).catch(error => {
      dispatch(loadNewsError(category, 'No se cargaron las noticias. Por favor revise su conexión.'));
      //throw(error);
    });
  };
}
export function resetNews(category) {
  return {
    type: RESET_NEWS,
    category: category,
  };
}

export function loadNewsFromGenerateUrl(category, id, params, reset) {  
  return function(dispatch) {
    dispatch(loadingNews(category, reset));
    return jsonApi.getAllFromGenerateUrl(id, params).then(news => {
      dispatch(loadNewsSuccess(category, news));
    }).catch(error => {
      dispatch(loadNewsError(category, error));
      //throw(error);
    });
  };
}
export function loadNewsSuccess(category, news, refresh) {
  return {
    type: LOAD_NEWS_SUCCESS,
    category: category,
    news: news,
    refresh
  };
}
export function loadingNews(category, refresh) {
  return {
    type: LOADING_NEWS,
    category: category,
    refresh
  };
}
export function loadNewsError(category, error) {
  return {
    type: LOAD_NEWS_ERROR,
    category: category,
    error: error
  };
}






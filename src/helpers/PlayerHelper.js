import { EventRegister } from "react-native-event-listeners";
import FloatingViewConstants from "../components/FloatingViewConstants";

class PlayerHelper {
  construct() {
    this.pause = null
    /*this.listener = EventRegister.addEventListener(FloatingViewConstants.layout.HIDDEN, ()=>{

    })*/
  }
  destructor(){
    //EventRegister.removeEventListener(this.listener)
  }
  register = (pause) => {
    if(pause === this.pause){
      return
    }
    if(this.pause){
      this.pause()
    }
    this.pause = pause
  }
  unregister = (pause) => {
    if(pause === this.pause){
      this.pause = null
    }
  }
}
let playerHelper = new PlayerHelper();
export default playerHelper;
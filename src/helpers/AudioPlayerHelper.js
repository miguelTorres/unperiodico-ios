class AudioPlayerHelper {
  construct() {
    this.pause = null
  }
  register = (pause) => {
    if(pause === this.pause){
      return
    }
    if(this.pause){
      this.pause()
    }
    this.pause = pause
  }
}
let audioPlayerHelper = new AudioPlayerHelper();
export default audioPlayerHelper;
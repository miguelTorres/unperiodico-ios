import _ from "lodash";



class ConfigurationHelper {
  getConfiguration = (configuration, categories) => {
    if(!configuration){
      return null
    }

    if(typeof configuration.categories === 'undefined'){
      return configuration.default
    }
    var result = null
    _.forEach(configuration.categories, (value, uid)=>{
      if(result){
        return
      }
      _.forEach(categories, (category)=>{
        if(result){
          return
        }
        if(uid == category){
          result = _.merge({}, configuration.default, value)
        }
  
      })
    })
    if(result === null){
      return configuration.default
    }
    return result
    
  }
  getTag = (configuration, category) => {
    return configuration.default.tag
  }


}
let configurationHelper = new ConfigurationHelper();
export default configurationHelper;
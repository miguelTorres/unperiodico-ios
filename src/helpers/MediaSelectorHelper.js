import _ from "lodash";
import appConfig from '../models/appConfig'
import DefaultSelector from '../models/DefaultSelector'
import AudioSelector from '../models/AudioSelector'
import VideoSelector from '../models/VideoSelector'
import IframeSelector from '../models/IframeSelector'
import PhotoGallerySelector from "../models/PhotoGallerySelector";

export default class MediaSelectorHelper {
    constructor(item, onlyDefault){
        this.item = item
        if(!!onlyDefault){
            this.selectors = [
                new DefaultSelector(item)
            ]    
        }else{
            this.selectors = [
                new VideoSelector(item), 
                new AudioSelector(item), 
                new IframeSelector(item), 
                new PhotoGallerySelector(item), 
                new DefaultSelector(item)
            ]    
        }
    }
    getSelector(){
        var selectorSelected = null
        _.forEach(this.selectors, (selector)=>{
            if(selector.isSelected()){
                selectorSelected = selector
                return false
            }
        })
        return selectorSelected
    }
}

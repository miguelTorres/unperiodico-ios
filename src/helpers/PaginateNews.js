class PaginateNews {
  resetNews = (newObject, category) => {
    newObject[category] = {
      items: [],
      pages: [],
      pagination: [],
      loading: false,
      error: null,
    }  

  }
  loadingNews = (newObject, category, refresh) => {
    
    if(typeof newObject[category] === 'undefined'){
      this.resetNews(newObject, category)
      newObject[category].initialLoading = true
    }else{
      newObject[category] = Object.assign({}, newObject[category]);
      newObject[category].initialLoading = false
    }
    if(refresh){
      newObject[category].refreshing = true
    }
  newObject[category].loading = true
    newObject[category].error = null

  }
  loadNewsError = (newObject, category, error) => {
    if(typeof newObject[category] === 'undefined'){
      this.resetNews(newObject, category)
    }else{
      newObject[category] = Object.assign({}, newObject[category]);
    }
    newObject[category].loading = false 
    newObject[category].refreshing = false
    newObject[category].error = error   
  }
  loadNews = (newObject, category, news, refresh) => {
    let items
    refresh = typeof refresh !== 'undefined' && refresh

    newObject[category] = Object.assign({}, newObject[category]);

    if(typeof news.news === 'undefined'){
      newObject[category].error = 'No hay noticias en esta sección'
      return;
    }
    if(refresh){
      this.resetNews(newObject, category)
    }

    if(newObject[category].items.length === 0){
      newObject[category].items = news.news
      newObject[category].pagination = news.pagination
      newObject[category].currentPage = 1
      newObject[category].initialLoading = true

      newObject[category].pages.push({
        ready: true,
        currentPage: 1
      })
    }else{
      items = newObject[category].items
      items = this.add(items, news.news)
      newObject[category].items = items
      newObject[category].pagination = news.pagination
      newObject[category].currentPage ++
      newObject[category].initialLoading = false
      newObject[category].pages.push({
        ready: true,
        currentPage: newObject[category].currentPage
      })
    }  
    newObject[category].nextPage = this.calculateAfterPage(newObject[category]);
    newObject[category].loading = false
    newObject[category].refreshing = false

  }
  add = (currentItems, newItems) => {
    return currentItems.concat(newItems)
  }
  calculateAfterPage = (config) => {
    if(typeof config.pagination === 'undefined'){
      return null
    }
    if(config.currentPage + 1 <= config.pagination.numberOfPages){
        return config.currentPage + 1;
    }
    return null;
  }

}
let paginateNews = new PaginateNews();
export default paginateNews;
import ApiUtils from './ApiUtils'
import GenerateUrl from './generateUrl'
import axios from 'axios';

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

class JsonApi {
  static getAll(url,options={}, attempts) {
    attempts = (!!attempts)? attempts : 4
    let fetchFunction = Object.keys(options).length > 0? JsonApi.getAllFetch : JsonApi.getAllFetch
    return fetchFunction(url,options, attempts).then(ApiUtils.checkStatus)
    .then(response => {
      return response.json();
    }).then(response => {
      return response;
    }).catch(async (error)=>{
      if ((!!error.response) && error.response.status == 503) {
        if(attempts === 1){
          throw error;
          return
        }
        await wait(3000)
        return JsonApi.getAll(url, options, attempts - 1)
      }else{
        throw error
      }
    });
  }

  static getAllOptions(url,options={}, attempts) {
    attempts = (!!attempts)? attempts : 4
    return fetch(url, options)
    .then(ApiUtils.checkStatus)
    .then(response => {
      return response.json();
    }).then(response => {
      return response;
    }).catch(async (error)=>{
      if ((!!error.response) && error.response.status == 503) {
        if(attempts === 1){
          throw error;
          return
        }
        await wait(3000)
        return JsonApi.getAll(url, options, attempts - 1)
      }else{
        throw error
      }
    });
  }

  static getAllAxios(url,options={}, attempts){
    return axios({
      method:'get', 
      url,
      headers: { 
        'Cache-Control': 'no-cache',
        'Expires': '0'
      },
      ...options
    })

  }
  static getAllFetch(url,options={}, attempts){
    var newoptions = {
      headers: {
        'Cache-Control': 'no-cache',
        'Expires': '0',
        ...options
      },
    }
    return fetch(url, newoptions)
  }

  static getAllFromGenerateUrl(id, params) {
    return GenerateUrl.generateUrl({
      id: id,
      parameters: params,
    }, 0)
    .then(response => response.json())
    .catch(error => {
      throw error;
    });
  }  
}

export default JsonApi;
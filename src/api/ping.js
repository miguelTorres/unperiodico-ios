import ApiUtils from './ApiUtils'
import appConfig from '../models/appConfig';
import axios from 'axios';
class Ping {
  static ping() {
    return axios({
      method: 'head',
      url: appConfig.config.app.PING_URL
    }).then((response)=>{
      if(response.status === 200){
        return true;
      }
      let error = new Error(response.statusText);
      error.response = response;
      throw error;
    });
  }

}

export default Ping;

import ApiUtils from './ApiUtils'
class AjaxApi {
  static getAll(url) {
    return fetch(url)
    .then(ApiUtils.checkStatus)
    .then(response => {
      return response.text();
    })
    .catch(error => {
      throw error;
    });
  }

}

export default AjaxApi;

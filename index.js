import React, { Component } from 'react'
import { AppRegistry, SafeAreaView, StyleSheet, View, Text } from 'react-native'
import { Provider } from 'react-redux'
import App from './src/containers/App'
import store from './src/store/configureStore'
import SplashScreen from 'react-native-splash-screen'
import FCM from './src/models/fcm'
import config from './config'

// Arreglo de setting a timer for a long period of time react native
import { YellowBox } from 'react-native';
import _ from 'lodash';

YellowBox.ignoreWarnings([
  'Setting a timer', 
  'Module RCTImageLoader requires main queue setup',
  'Class RCTCxxxModule was not exported',
  ]);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};




class UNPeriodico extends Component {
  componentDidMount() {
    // do stuff while splash screen is shown
      // After having done stuff (such as async tasks) hide the splash screen
      SplashScreen.hide();
      FCM.init()

  }  
  componentWillUnmount(){
    FCM.finalize()
  }
  render() {
    return (
      <SafeAreaView style={styles.safeArea}>
       <Provider store={store}>
        <App />
      </Provider>
     </SafeAreaView>
    )
  }
}
const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: config.colors.config1.background
  }
})
AppRegistry.registerComponent('UNPeriodico', () => UNPeriodico);

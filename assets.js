module.exports = {

    // Parámetros para el menú superior
    logo: require('./assets/logo-unperiodico.png'),
    logoInvertido: require('./assets/logo-unperiodico-invertido.png'),
    flechaRojaAbajo: require('./assets/icons/app-iconflecha-roja175.png'),
    flechaAmarillaArriba: require('./assets/icons/app-iconflecha-amarilla175.png'),
    flechaBlancaArriba: require('./assets/icons/app-iconflecha-blanca_1175.png'),
    flechaBlancaAbajo: require('./assets/icons/app-iconflecha-blanca175.png'),
    
    filtroFecha: require('./assets/icons/app-iconfiltro175.png'),
    flechaAtras: require('./assets/icons/app-iconatras175.png'),

    campana: require('./assets/icons/app-iconcampana175.png'),
    campanaSilencio: require('./assets/icons/app-iconcampanasilencio175.png'),
    equisRoja: require('./assets/icons/app-iconequisroja175.png'),
    
    mas: require('./assets/icons/app-iconver-mas175.png'),
    play: require('./assets/icons/app-iconplay-tojo175.png'),
    pause: require('./assets/icons/app-iconpausa-rojo175.png'),

    indicador: require('./assets/icons/indicador.png'),
    indicadorInactivo: require('./assets/icons/indicadorinactivo.png'),
    indicatorYellow: require('./assets/icons/indicator-yellow.png'),
    indicatorWhite: require('./assets/icons/indicator-white.png'),
    indicatorBlack: require('./assets/icons/indicator-black.png'),
    indicatorGray: require('./assets/icons/indicator-gray.png'),
    indicatorClaret: require('./assets/icons/indicator-claret.png'),
};

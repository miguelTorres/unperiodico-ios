import variable from "./../variables/unperiodico-material";

export default (variables = variable) => {
  const iconTheme = {
    fontSize: variables.iconFontSize,
    color: "#000"
  };

  return iconTheme;
};

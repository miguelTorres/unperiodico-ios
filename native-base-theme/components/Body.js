import variable from './../variables/unperiodico-material';

export default (variables = variable) => {
	const bodyTheme = {
		flex: 1,
		alignItems: 'center',
		alignSelf: 'center',
	};

	return bodyTheme;
};

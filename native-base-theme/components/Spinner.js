import variable from "./../variables/unperiodico-material";

export default (variables = variable) => {
  const spinnerTheme = {
    height: 80
  };

  return spinnerTheme;
};

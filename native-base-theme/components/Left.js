import variable from './../variables/unperiodico-material';

export default (variables = variable) => {
	const leftTheme = {
		flex: 1,
		alignSelf: 'center',
		alignItems: 'flex-start',
	};

	return leftTheme;
};

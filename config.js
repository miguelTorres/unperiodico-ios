import { Platform } from 'react-native'
import { calculateResponsiveValue } from './src/helpers/Responsive';
const isProduction = true;
// See https://firebase.google.com/docs/web/setup#project_setup
module.exports = {
  // Firebase settings
  PRODUCTION: isProduction,
  API_KEY: "AIzaSyB1sajIjAzjQWP6a1ETUB16_R-hBq3XUhg",
  AUTH_DOMAIN: "groceries-54bbb.firebaseapp.com",
  DATABASE_URL: "https://groceries-54bbb.firebaseio.com",
  STORAGE_BUCKET: "groceries-54bbb.appspot.com",

  //CONFIG_URL: 'https://dev3.unal.edu.co/pages/json/app-config/?type=101',
  CONFIG_URL: isProduction? 'https://unperiodico.unal.edu.co/pages/json/app-config/?type=101':
  'https://dev3.unal.edu.co/pages/json/app-config/?type=101',
  //CONFIG_URL: 'https://dev3.unal.edu.co/pages/json/app-config/?type=101',
  // Configuración general
  fonts: {
    'opensans': Platform.OS === 'android' ? 'opensans' : 'Open Sans',
    'opensansitalic': Platform.OS === 'android' ? 'opensans_bold_italic' : 'Open Sans',
  },
  colors: {
    config1: {
      color: 'white',
      background: 'rgba(128,24,54,1)',
    },
    config2: {
      color: 'rgba(128,24,54,1)',
      background: 'rgba(255,210,98,1)'
    },
    config3: {
      background: '#414240',
      color: '#fff',
    }

  },
  filtroFecha: {
    height: calculateResponsiveValue(50)
  },
  carouselRelated: {
    height: calculateResponsiveValue(185)
  }

}

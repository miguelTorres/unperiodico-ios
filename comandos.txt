Command line instructions


Git global setup

git config --global user.name "Pedro Santiago Murillo Moreno"
git config --global user.email "psmurillom@unal.edu.co"

Create a new repository

git clone http://168.176.236.13:8081/unperiodico-movil/unperiodico-rn.git
cd unperiodico-rn
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin http://168.176.236.13:8081/unperiodico-movil/unperiodico-rn.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin http://168.176.236.13:8081/unperiodico-movil/unperiodico-rn.git
git push -u origin --all
git push -u origin --tags
